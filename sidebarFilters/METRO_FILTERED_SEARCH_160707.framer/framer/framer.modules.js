require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"TextLayer":[function(require,module,exports){
var TextLayer, convertTextLayers, convertToTextLayer,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

TextLayer = (function(superClass) {
  extend(TextLayer, superClass);

  function TextLayer(options) {
    if (options == null) {
      options = {};
    }
    this.doAutoSize = false;
    this.doAutoSizeHeight = false;
    if (options.backgroundColor == null) {
      options.backgroundColor = options.setup ? "hsla(60, 90%, 47%, .4)" : "transparent";
    }
    if (options.color == null) {
      options.color = "red";
    }
    if (options.lineHeight == null) {
      options.lineHeight = 1.25;
    }
    if (options.fontFamily == null) {
      options.fontFamily = "Helvetica";
    }
    if (options.fontSize == null) {
      options.fontSize = 20;
    }
    if (options.text == null) {
      options.text = "Use layer.text to add text";
    }
    TextLayer.__super__.constructor.call(this, options);
    this.style.whiteSpace = "pre-line";
    this.style.outline = "none";
  }

  TextLayer.prototype.setStyle = function(property, value, pxSuffix) {
    if (pxSuffix == null) {
      pxSuffix = false;
    }
    this.style[property] = pxSuffix ? value + "px" : value;
    this.emit("change:" + property, value);
    if (this.doAutoSize) {
      return this.calcSize();
    }
  };

  TextLayer.prototype.calcSize = function() {
    var constraints, size, sizeAffectingStyles;
    sizeAffectingStyles = {
      lineHeight: this.style["line-height"],
      fontSize: this.style["font-size"],
      fontWeight: this.style["font-weight"],
      paddingTop: this.style["padding-top"],
      paddingRight: this.style["padding-right"],
      paddingBottom: this.style["padding-bottom"],
      paddingLeft: this.style["padding-left"],
      textTransform: this.style["text-transform"],
      borderWidth: this.style["border-width"],
      letterSpacing: this.style["letter-spacing"],
      fontFamily: this.style["font-family"],
      fontStyle: this.style["font-style"],
      fontVariant: this.style["font-variant"]
    };
    constraints = {};
    if (this.doAutoSizeHeight) {
      constraints.width = this.width;
    }
    size = Utils.textSize(this.text, sizeAffectingStyles, constraints);
    if (this.style.textAlign === "right") {
      this.width = size.width;
      this.x = this.x - this.width;
    } else {
      this.width = size.width;
    }
    return this.height = size.height;
  };

  TextLayer.define("autoSize", {
    get: function() {
      return this.doAutoSize;
    },
    set: function(value) {
      this.doAutoSize = value;
      if (this.doAutoSize) {
        return this.calcSize();
      }
    }
  });

  TextLayer.define("autoSizeHeight", {
    set: function(value) {
      this.doAutoSize = value;
      this.doAutoSizeHeight = value;
      if (this.doAutoSize) {
        return this.calcSize();
      }
    }
  });

  TextLayer.define("contentEditable", {
    set: function(boolean) {
      this._element.contentEditable = boolean;
      this.ignoreEvents = !boolean;
      return this.on("input", function() {
        if (this.doAutoSize) {
          return this.calcSize();
        }
      });
    }
  });

  TextLayer.define("text", {
    get: function() {
      return this._element.textContent;
    },
    set: function(value) {
      this._element.textContent = value;
      this.emit("change:text", value);
      if (this.doAutoSize) {
        return this.calcSize();
      }
    }
  });

  TextLayer.define("fontFamily", {
    get: function() {
      return this.style.fontFamily;
    },
    set: function(value) {
      return this.setStyle("fontFamily", value);
    }
  });

  TextLayer.define("fontSize", {
    get: function() {
      return this.style.fontSize.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("fontSize", value, true);
    }
  });

  TextLayer.define("lineHeight", {
    get: function() {
      return this.style.lineHeight;
    },
    set: function(value) {
      return this.setStyle("lineHeight", value);
    }
  });

  TextLayer.define("fontWeight", {
    get: function() {
      return this.style.fontWeight;
    },
    set: function(value) {
      return this.setStyle("fontWeight", value);
    }
  });

  TextLayer.define("fontStyle", {
    get: function() {
      return this.style.fontStyle;
    },
    set: function(value) {
      return this.setStyle("fontStyle", value);
    }
  });

  TextLayer.define("fontVariant", {
    get: function() {
      return this.style.fontVariant;
    },
    set: function(value) {
      return this.setStyle("fontVariant", value);
    }
  });

  TextLayer.define("padding", {
    set: function(value) {
      this.setStyle("paddingTop", value, true);
      this.setStyle("paddingRight", value, true);
      this.setStyle("paddingBottom", value, true);
      return this.setStyle("paddingLeft", value, true);
    }
  });

  TextLayer.define("paddingTop", {
    get: function() {
      return this.style.paddingTop.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("paddingTop", value, true);
    }
  });

  TextLayer.define("paddingRight", {
    get: function() {
      return this.style.paddingRight.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("paddingRight", value, true);
    }
  });

  TextLayer.define("paddingBottom", {
    get: function() {
      return this.style.paddingBottom.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("paddingBottom", value, true);
    }
  });

  TextLayer.define("paddingLeft", {
    get: function() {
      return this.style.paddingLeft.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("paddingLeft", value, true);
    }
  });

  TextLayer.define("textAlign", {
    set: function(value) {
      return this.setStyle("textAlign", value);
    }
  });

  TextLayer.define("textTransform", {
    get: function() {
      return this.style.textTransform;
    },
    set: function(value) {
      return this.setStyle("textTransform", value);
    }
  });

  TextLayer.define("letterSpacing", {
    get: function() {
      return this.style.letterSpacing.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("letterSpacing", value, true);
    }
  });

  TextLayer.define("length", {
    get: function() {
      return this.text.length;
    }
  });

  return TextLayer;

})(Layer);

convertToTextLayer = function(layer) {
  var css, cssObj, importPath, t;
  t = new TextLayer({
    name: layer.name,
    frame: layer.frame,
    parent: layer.parent
  });
  cssObj = {};
  css = layer._info.metadata.css;
  css.forEach(function(rule) {
    var arr;
    if (_.includes(rule, '/*')) {
      return;
    }
    arr = rule.split(': ');
    return cssObj[arr[0]] = arr[1].replace(';', '');
  });
  t.style = cssObj;
  importPath = layer.__framerImportedFromPath;
  if (_.includes(importPath, '@2x')) {
    t.fontSize *= 2;
    t.lineHeight = (parseInt(t.lineHeight) * 2) + 'px';
    t.letterSpacing *= 2;
  }
  t.y -= (parseInt(t.lineHeight) - t.fontSize) / 2;
  t.y -= t.fontSize * 0.1;
  t.x -= t.fontSize * 0.08;
  t.width += t.fontSize * 0.5;
  t.text = layer._info.metadata.string;
  layer.destroy();
  return t;
};

Layer.prototype.convertToTextLayer = function() {
  return convertToTextLayer(this);
};

convertTextLayers = function(obj) {
  var layer, prop, results;
  results = [];
  for (prop in obj) {
    layer = obj[prop];
    if (layer._info.kind === "text") {
      results.push(obj[prop] = convertToTextLayer(layer));
    } else {
      results.push(void 0);
    }
  }
  return results;
};

Layer.prototype.frameAsTextLayer = function(properties) {
  var t;
  t = new TextLayer;
  t.frame = this.frame;
  t.superLayer = this.superLayer;
  _.extend(t, properties);
  this.destroy();
  return t;
};

exports.TextLayer = TextLayer;

exports.convertTextLayers = convertTextLayers;


},{}],"fluid-framer":[function(require,module,exports){
var FluidFramer;

Layer.prototype.fluid = function(options) {
  if (options == null) {
    options = {};
  }
  return Framer.Fluid.register(this, options);
};

Layer.prototype["static"] = function() {
  return Framer.Fluid.unregister(this);
};

Layer.prototype.fix = function() {
  return Framer.Fluid.fix(this);
};

Layer.prototype.unfix = function() {
  return Framer.Fluid.unfix(this);
};

FluidFramer = (function() {
  var registry;

  registry = [];

  function FluidFramer() {
    var self;
    self = this;
    window.onresize = (function(_this) {
      return function(evt) {
        return _this._respond();
      };
    })(this);
  }

  FluidFramer.prototype.register = function(layer, options) {
    if (options == null) {
      options = {};
    }
    return this._addLayer(layer, options);
  };

  FluidFramer.prototype.unregister = function(layer) {
    return this._removeLayer(layer);
  };

  FluidFramer.prototype.fix = function(layer) {
    layer.style = {
      position: 'fixed'
    };
    return layer;
  };

  FluidFramer.prototype.unfix = function(layer) {
    layer.style = {
      position: 'absolute'
    };
    return layer;
  };

  FluidFramer.prototype.layers = function() {
    return registry;
  };

  FluidFramer.prototype._respond = function() {
    var self;
    self = this;
    return _.each(registry, function(obj, index) {
      return self._refreshLayer(obj);
    });
  };

  FluidFramer.prototype._refreshLayer = function(obj) {
    var layer, newHeight, newWidth, newX, newY;
    layer = obj.targetLayer;
    if (obj.autoWidth != null) {
      newWidth = obj.widthOffset != null ? this._parentWidth(layer) + obj.widthOffset : this._parentWidth(layer);
      layer.width = newWidth;
      layer.style = {
        backgroundPosition: 'center'
      };
    }
    if (obj.autoHeight != null) {
      newHeight = obj.heightOffset != null ? this._parentHeight(layer) + obj.heightOffset : this._parentHeight(layer);
      layer.height = newHeight;
      layer.style = {
        backgroundPosition: 'center'
      };
    }
    switch (obj.xAlign) {
      case 'left':
        layer.x = this._xWithOffset(obj, 0);
        break;
      case 'right':
        newX = this._parentWidth(layer) - layer.width;
        layer.x = this._xWithOffset(obj, newX);
        break;
      case 'center':
        layer.centerX();
        layer.x = this._xWithOffset(obj, layer.x);
        break;
      case 'middle':
        layer.centerX();
        layer.x = this._xWithOffset(obj, layer.x);
    }
    switch (obj.yAlign) {
      case 'bottom':
        newY = this._parentHeight(layer) - layer.height;
        return layer.y = this._yWithOffset(obj, newY);
      case 'top':
        return layer.y = this._yWithOffset(obj, 0);
      case 'middle':
        layer.centerY();
        return layer.y = this._yWithOffset(obj, layer.y);
      case 'center':
        layer.centerY();
        return layer.y = this._yWithOffset(obj, layer.y);
    }
  };

  FluidFramer.prototype._xWithOffset = function(obj, x) {
    return x = obj.xOffset != null ? x + obj.xOffset : x;
  };

  FluidFramer.prototype._yWithOffset = function(obj, y) {
    return y = obj.yOffset != null ? y + obj.yOffset : y;
  };

  FluidFramer.prototype._parentWidth = function(layer) {
    if (layer.superLayer != null) {
      return layer.superLayer.width;
    } else {
      return window.innerWidth;
    }
  };

  FluidFramer.prototype._parentHeight = function(layer) {
    if (layer.superLayer != null) {
      return layer.superLayer.height;
    } else {
      return window.innerHeight;
    }
  };

  FluidFramer.prototype._addLayer = function(layer, options) {
    var obj, self;
    if (options == null) {
      options = {};
    }
    obj = _.extend(options, {
      targetLayer: layer
    });
    registry.push(obj);
    self = this;
    Utils.domComplete(function() {
      return self._refreshLayer(obj, self);
    });
    return layer;
  };

  FluidFramer.prototype._removeLayer = function(layer) {
    var target;
    target = _.findWhere(registry, {
      targetLayer: layer
    });
    if (target == null) {
      return layer;
    }
    if ((target.autoWidth != null) || (target.autoHeight != null)) {
      target.style = {
        backgroundPosition: 'initial'
      };
    }
    registry = _.without(registry, target);
    return target;
  };

  return FluidFramer;

})();

Framer.Fluid = new FluidFramer;


},{}]},{},[])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvVXNlcnMvdGlsbGhpbnJpY2hzL21ldHJvLWJldHR5LmJpdGJ1Y2tldC5vcmcvc2lkZWJhckZpbHRlcnMvTUVUUk9fRklMVEVSRURfU0VBUkNIXzE2MDcwNy5mcmFtZXIvbW9kdWxlcy9UZXh0TGF5ZXIuY29mZmVlIiwiL1VzZXJzL3RpbGxoaW5yaWNocy9tZXRyby1iZXR0eS5iaXRidWNrZXQub3JnL3NpZGViYXJGaWx0ZXJzL01FVFJPX0ZJTFRFUkVEX1NFQVJDSF8xNjA3MDcuZnJhbWVyL21vZHVsZXMvZmx1aWQtZnJhbWVyLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBLElBQUEsZ0RBQUE7RUFBQTs7O0FBQU07OztFQUVRLG1CQUFDLE9BQUQ7O01BQUMsVUFBUTs7SUFDckIsSUFBQyxDQUFBLFVBQUQsR0FBYztJQUNkLElBQUMsQ0FBQSxnQkFBRCxHQUFvQjs7TUFDcEIsT0FBTyxDQUFDLGtCQUFzQixPQUFPLENBQUMsS0FBWCxHQUFzQix3QkFBdEIsR0FBb0Q7OztNQUMvRSxPQUFPLENBQUMsUUFBUzs7O01BQ2pCLE9BQU8sQ0FBQyxhQUFjOzs7TUFDdEIsT0FBTyxDQUFDLGFBQWM7OztNQUN0QixPQUFPLENBQUMsV0FBWTs7O01BQ3BCLE9BQU8sQ0FBQyxPQUFROztJQUNoQiwyQ0FBTSxPQUFOO0lBQ0EsSUFBQyxDQUFBLEtBQUssQ0FBQyxVQUFQLEdBQW9CO0lBQ3BCLElBQUMsQ0FBQSxLQUFLLENBQUMsT0FBUCxHQUFpQjtFQVhMOztzQkFhYixRQUFBLEdBQVUsU0FBQyxRQUFELEVBQVcsS0FBWCxFQUFrQixRQUFsQjs7TUFBa0IsV0FBVzs7SUFDdEMsSUFBQyxDQUFBLEtBQU0sQ0FBQSxRQUFBLENBQVAsR0FBc0IsUUFBSCxHQUFpQixLQUFBLEdBQU0sSUFBdkIsR0FBaUM7SUFDcEQsSUFBQyxDQUFBLElBQUQsQ0FBTSxTQUFBLEdBQVUsUUFBaEIsRUFBNEIsS0FBNUI7SUFDQSxJQUFHLElBQUMsQ0FBQSxVQUFKO2FBQW9CLElBQUMsQ0FBQSxRQUFELENBQUEsRUFBcEI7O0VBSFM7O3NCQUtWLFFBQUEsR0FBVSxTQUFBO0FBQ1QsUUFBQTtJQUFBLG1CQUFBLEdBQ0M7TUFBQSxVQUFBLEVBQVksSUFBQyxDQUFBLEtBQU0sQ0FBQSxhQUFBLENBQW5CO01BQ0EsUUFBQSxFQUFVLElBQUMsQ0FBQSxLQUFNLENBQUEsV0FBQSxDQURqQjtNQUVBLFVBQUEsRUFBWSxJQUFDLENBQUEsS0FBTSxDQUFBLGFBQUEsQ0FGbkI7TUFHQSxVQUFBLEVBQVksSUFBQyxDQUFBLEtBQU0sQ0FBQSxhQUFBLENBSG5CO01BSUEsWUFBQSxFQUFjLElBQUMsQ0FBQSxLQUFNLENBQUEsZUFBQSxDQUpyQjtNQUtBLGFBQUEsRUFBZSxJQUFDLENBQUEsS0FBTSxDQUFBLGdCQUFBLENBTHRCO01BTUEsV0FBQSxFQUFhLElBQUMsQ0FBQSxLQUFNLENBQUEsY0FBQSxDQU5wQjtNQU9BLGFBQUEsRUFBZSxJQUFDLENBQUEsS0FBTSxDQUFBLGdCQUFBLENBUHRCO01BUUEsV0FBQSxFQUFhLElBQUMsQ0FBQSxLQUFNLENBQUEsY0FBQSxDQVJwQjtNQVNBLGFBQUEsRUFBZSxJQUFDLENBQUEsS0FBTSxDQUFBLGdCQUFBLENBVHRCO01BVUEsVUFBQSxFQUFZLElBQUMsQ0FBQSxLQUFNLENBQUEsYUFBQSxDQVZuQjtNQVdBLFNBQUEsRUFBVyxJQUFDLENBQUEsS0FBTSxDQUFBLFlBQUEsQ0FYbEI7TUFZQSxXQUFBLEVBQWEsSUFBQyxDQUFBLEtBQU0sQ0FBQSxjQUFBLENBWnBCOztJQWFELFdBQUEsR0FBYztJQUNkLElBQUcsSUFBQyxDQUFBLGdCQUFKO01BQTBCLFdBQVcsQ0FBQyxLQUFaLEdBQW9CLElBQUMsQ0FBQSxNQUEvQzs7SUFDQSxJQUFBLEdBQU8sS0FBSyxDQUFDLFFBQU4sQ0FBZSxJQUFDLENBQUEsSUFBaEIsRUFBc0IsbUJBQXRCLEVBQTJDLFdBQTNDO0lBQ1AsSUFBRyxJQUFDLENBQUEsS0FBSyxDQUFDLFNBQVAsS0FBb0IsT0FBdkI7TUFDQyxJQUFDLENBQUEsS0FBRCxHQUFTLElBQUksQ0FBQztNQUNkLElBQUMsQ0FBQSxDQUFELEdBQUssSUFBQyxDQUFBLENBQUQsR0FBRyxJQUFDLENBQUEsTUFGVjtLQUFBLE1BQUE7TUFJQyxJQUFDLENBQUEsS0FBRCxHQUFTLElBQUksQ0FBQyxNQUpmOztXQUtBLElBQUMsQ0FBQSxNQUFELEdBQVUsSUFBSSxDQUFDO0VBdkJOOztFQXlCVixTQUFDLENBQUEsTUFBRCxDQUFRLFVBQVIsRUFDQztJQUFBLEdBQUEsRUFBSyxTQUFBO2FBQUcsSUFBQyxDQUFBO0lBQUosQ0FBTDtJQUNBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7TUFDSixJQUFDLENBQUEsVUFBRCxHQUFjO01BQ2QsSUFBRyxJQUFDLENBQUEsVUFBSjtlQUFvQixJQUFDLENBQUEsUUFBRCxDQUFBLEVBQXBCOztJQUZJLENBREw7R0FERDs7RUFLQSxTQUFDLENBQUEsTUFBRCxDQUFRLGdCQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQyxLQUFEO01BQ0osSUFBQyxDQUFBLFVBQUQsR0FBYztNQUNkLElBQUMsQ0FBQSxnQkFBRCxHQUFvQjtNQUNwQixJQUFHLElBQUMsQ0FBQSxVQUFKO2VBQW9CLElBQUMsQ0FBQSxRQUFELENBQUEsRUFBcEI7O0lBSEksQ0FBTDtHQUREOztFQUtBLFNBQUMsQ0FBQSxNQUFELENBQVEsaUJBQVIsRUFDQztJQUFBLEdBQUEsRUFBSyxTQUFDLE9BQUQ7TUFDSixJQUFDLENBQUEsUUFBUSxDQUFDLGVBQVYsR0FBNEI7TUFDNUIsSUFBQyxDQUFBLFlBQUQsR0FBZ0IsQ0FBQzthQUNqQixJQUFDLENBQUEsRUFBRCxDQUFJLE9BQUosRUFBYSxTQUFBO1FBQUcsSUFBZSxJQUFDLENBQUEsVUFBaEI7aUJBQUEsSUFBQyxDQUFBLFFBQUQsQ0FBQSxFQUFBOztNQUFILENBQWI7SUFISSxDQUFMO0dBREQ7O0VBS0EsU0FBQyxDQUFBLE1BQUQsQ0FBUSxNQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQSxRQUFRLENBQUM7SUFBYixDQUFMO0lBQ0EsR0FBQSxFQUFLLFNBQUMsS0FBRDtNQUNKLElBQUMsQ0FBQSxRQUFRLENBQUMsV0FBVixHQUF3QjtNQUN4QixJQUFDLENBQUEsSUFBRCxDQUFNLGFBQU4sRUFBcUIsS0FBckI7TUFDQSxJQUFHLElBQUMsQ0FBQSxVQUFKO2VBQW9CLElBQUMsQ0FBQSxRQUFELENBQUEsRUFBcEI7O0lBSEksQ0FETDtHQUREOztFQU1BLFNBQUMsQ0FBQSxNQUFELENBQVEsWUFBUixFQUNDO0lBQUEsR0FBQSxFQUFLLFNBQUE7YUFBRyxJQUFDLENBQUEsS0FBSyxDQUFDO0lBQVYsQ0FBTDtJQUNBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7YUFBVyxJQUFDLENBQUEsUUFBRCxDQUFVLFlBQVYsRUFBd0IsS0FBeEI7SUFBWCxDQURMO0dBREQ7O0VBR0EsU0FBQyxDQUFBLE1BQUQsQ0FBUSxVQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQSxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQWhCLENBQXdCLElBQXhCLEVBQTZCLEVBQTdCO0lBQUgsQ0FBTDtJQUNBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7YUFBVyxJQUFDLENBQUEsUUFBRCxDQUFVLFVBQVYsRUFBc0IsS0FBdEIsRUFBNkIsSUFBN0I7SUFBWCxDQURMO0dBREQ7O0VBR0EsU0FBQyxDQUFBLE1BQUQsQ0FBUSxZQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQSxLQUFLLENBQUM7SUFBVixDQUFMO0lBQ0EsR0FBQSxFQUFLLFNBQUMsS0FBRDthQUFXLElBQUMsQ0FBQSxRQUFELENBQVUsWUFBVixFQUF3QixLQUF4QjtJQUFYLENBREw7R0FERDs7RUFHQSxTQUFDLENBQUEsTUFBRCxDQUFRLFlBQVIsRUFDQztJQUFBLEdBQUEsRUFBSyxTQUFBO2FBQUcsSUFBQyxDQUFBLEtBQUssQ0FBQztJQUFWLENBQUw7SUFDQSxHQUFBLEVBQUssU0FBQyxLQUFEO2FBQVcsSUFBQyxDQUFBLFFBQUQsQ0FBVSxZQUFWLEVBQXdCLEtBQXhCO0lBQVgsQ0FETDtHQUREOztFQUdBLFNBQUMsQ0FBQSxNQUFELENBQVEsV0FBUixFQUNDO0lBQUEsR0FBQSxFQUFLLFNBQUE7YUFBRyxJQUFDLENBQUEsS0FBSyxDQUFDO0lBQVYsQ0FBTDtJQUNBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7YUFBVyxJQUFDLENBQUEsUUFBRCxDQUFVLFdBQVYsRUFBdUIsS0FBdkI7SUFBWCxDQURMO0dBREQ7O0VBR0EsU0FBQyxDQUFBLE1BQUQsQ0FBUSxhQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQSxLQUFLLENBQUM7SUFBVixDQUFMO0lBQ0EsR0FBQSxFQUFLLFNBQUMsS0FBRDthQUFXLElBQUMsQ0FBQSxRQUFELENBQVUsYUFBVixFQUF5QixLQUF6QjtJQUFYLENBREw7R0FERDs7RUFHQSxTQUFDLENBQUEsTUFBRCxDQUFRLFNBQVIsRUFDQztJQUFBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7TUFDSixJQUFDLENBQUEsUUFBRCxDQUFVLFlBQVYsRUFBd0IsS0FBeEIsRUFBK0IsSUFBL0I7TUFDQSxJQUFDLENBQUEsUUFBRCxDQUFVLGNBQVYsRUFBMEIsS0FBMUIsRUFBaUMsSUFBakM7TUFDQSxJQUFDLENBQUEsUUFBRCxDQUFVLGVBQVYsRUFBMkIsS0FBM0IsRUFBa0MsSUFBbEM7YUFDQSxJQUFDLENBQUEsUUFBRCxDQUFVLGFBQVYsRUFBeUIsS0FBekIsRUFBZ0MsSUFBaEM7SUFKSSxDQUFMO0dBREQ7O0VBTUEsU0FBQyxDQUFBLE1BQUQsQ0FBUSxZQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQSxLQUFLLENBQUMsVUFBVSxDQUFDLE9BQWxCLENBQTBCLElBQTFCLEVBQStCLEVBQS9CO0lBQUgsQ0FBTDtJQUNBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7YUFBVyxJQUFDLENBQUEsUUFBRCxDQUFVLFlBQVYsRUFBd0IsS0FBeEIsRUFBK0IsSUFBL0I7SUFBWCxDQURMO0dBREQ7O0VBR0EsU0FBQyxDQUFBLE1BQUQsQ0FBUSxjQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQSxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQXBCLENBQTRCLElBQTVCLEVBQWlDLEVBQWpDO0lBQUgsQ0FBTDtJQUNBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7YUFBVyxJQUFDLENBQUEsUUFBRCxDQUFVLGNBQVYsRUFBMEIsS0FBMUIsRUFBaUMsSUFBakM7SUFBWCxDQURMO0dBREQ7O0VBR0EsU0FBQyxDQUFBLE1BQUQsQ0FBUSxlQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQSxLQUFLLENBQUMsYUFBYSxDQUFDLE9BQXJCLENBQTZCLElBQTdCLEVBQWtDLEVBQWxDO0lBQUgsQ0FBTDtJQUNBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7YUFBVyxJQUFDLENBQUEsUUFBRCxDQUFVLGVBQVYsRUFBMkIsS0FBM0IsRUFBa0MsSUFBbEM7SUFBWCxDQURMO0dBREQ7O0VBR0EsU0FBQyxDQUFBLE1BQUQsQ0FBUSxhQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQSxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQW5CLENBQTJCLElBQTNCLEVBQWdDLEVBQWhDO0lBQUgsQ0FBTDtJQUNBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7YUFBVyxJQUFDLENBQUEsUUFBRCxDQUFVLGFBQVYsRUFBeUIsS0FBekIsRUFBZ0MsSUFBaEM7SUFBWCxDQURMO0dBREQ7O0VBR0EsU0FBQyxDQUFBLE1BQUQsQ0FBUSxXQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQyxLQUFEO2FBQVcsSUFBQyxDQUFBLFFBQUQsQ0FBVSxXQUFWLEVBQXVCLEtBQXZCO0lBQVgsQ0FBTDtHQUREOztFQUVBLFNBQUMsQ0FBQSxNQUFELENBQVEsZUFBUixFQUNDO0lBQUEsR0FBQSxFQUFLLFNBQUE7YUFBRyxJQUFDLENBQUEsS0FBSyxDQUFDO0lBQVYsQ0FBTDtJQUNBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7YUFBVyxJQUFDLENBQUEsUUFBRCxDQUFVLGVBQVYsRUFBMkIsS0FBM0I7SUFBWCxDQURMO0dBREQ7O0VBR0EsU0FBQyxDQUFBLE1BQUQsQ0FBUSxlQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQSxLQUFLLENBQUMsYUFBYSxDQUFDLE9BQXJCLENBQTZCLElBQTdCLEVBQWtDLEVBQWxDO0lBQUgsQ0FBTDtJQUNBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7YUFBVyxJQUFDLENBQUEsUUFBRCxDQUFVLGVBQVYsRUFBMkIsS0FBM0IsRUFBa0MsSUFBbEM7SUFBWCxDQURMO0dBREQ7O0VBR0EsU0FBQyxDQUFBLE1BQUQsQ0FBUSxRQUFSLEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQSxJQUFJLENBQUM7SUFBVCxDQUFMO0dBREQ7Ozs7R0E5R3VCOztBQWlIeEIsa0JBQUEsR0FBcUIsU0FBQyxLQUFEO0FBQ3BCLE1BQUE7RUFBQSxDQUFBLEdBQVEsSUFBQSxTQUFBLENBQ1A7SUFBQSxJQUFBLEVBQU0sS0FBSyxDQUFDLElBQVo7SUFDQSxLQUFBLEVBQU8sS0FBSyxDQUFDLEtBRGI7SUFFQSxNQUFBLEVBQVEsS0FBSyxDQUFDLE1BRmQ7R0FETztFQUtSLE1BQUEsR0FBUztFQUNULEdBQUEsR0FBTSxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztFQUMzQixHQUFHLENBQUMsT0FBSixDQUFZLFNBQUMsSUFBRDtBQUNYLFFBQUE7SUFBQSxJQUFVLENBQUMsQ0FBQyxRQUFGLENBQVcsSUFBWCxFQUFpQixJQUFqQixDQUFWO0FBQUEsYUFBQTs7SUFDQSxHQUFBLEdBQU0sSUFBSSxDQUFDLEtBQUwsQ0FBVyxJQUFYO1dBQ04sTUFBTyxDQUFBLEdBQUksQ0FBQSxDQUFBLENBQUosQ0FBUCxHQUFpQixHQUFJLENBQUEsQ0FBQSxDQUFFLENBQUMsT0FBUCxDQUFlLEdBQWYsRUFBbUIsRUFBbkI7RUFITixDQUFaO0VBSUEsQ0FBQyxDQUFDLEtBQUYsR0FBVTtFQUVWLFVBQUEsR0FBYSxLQUFLLENBQUM7RUFDbkIsSUFBRyxDQUFDLENBQUMsUUFBRixDQUFXLFVBQVgsRUFBdUIsS0FBdkIsQ0FBSDtJQUNDLENBQUMsQ0FBQyxRQUFGLElBQWM7SUFDZCxDQUFDLENBQUMsVUFBRixHQUFlLENBQUMsUUFBQSxDQUFTLENBQUMsQ0FBQyxVQUFYLENBQUEsR0FBdUIsQ0FBeEIsQ0FBQSxHQUEyQjtJQUMxQyxDQUFDLENBQUMsYUFBRixJQUFtQixFQUhwQjs7RUFLQSxDQUFDLENBQUMsQ0FBRixJQUFPLENBQUMsUUFBQSxDQUFTLENBQUMsQ0FBQyxVQUFYLENBQUEsR0FBdUIsQ0FBQyxDQUFDLFFBQTFCLENBQUEsR0FBb0M7RUFDM0MsQ0FBQyxDQUFDLENBQUYsSUFBTyxDQUFDLENBQUMsUUFBRixHQUFhO0VBQ3BCLENBQUMsQ0FBQyxDQUFGLElBQU8sQ0FBQyxDQUFDLFFBQUYsR0FBYTtFQUNwQixDQUFDLENBQUMsS0FBRixJQUFXLENBQUMsQ0FBQyxRQUFGLEdBQWE7RUFFeEIsQ0FBQyxDQUFDLElBQUYsR0FBUyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztFQUM5QixLQUFLLENBQUMsT0FBTixDQUFBO0FBQ0EsU0FBTztBQTNCYTs7QUE2QnJCLEtBQUssQ0FBQSxTQUFFLENBQUEsa0JBQVAsR0FBNEIsU0FBQTtTQUFHLGtCQUFBLENBQW1CLElBQW5CO0FBQUg7O0FBRTVCLGlCQUFBLEdBQW9CLFNBQUMsR0FBRDtBQUNuQixNQUFBO0FBQUE7T0FBQSxXQUFBOztJQUNDLElBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFaLEtBQW9CLE1BQXZCO21CQUNDLEdBQUksQ0FBQSxJQUFBLENBQUosR0FBWSxrQkFBQSxDQUFtQixLQUFuQixHQURiO0tBQUEsTUFBQTsyQkFBQTs7QUFERDs7QUFEbUI7O0FBTXBCLEtBQUssQ0FBQSxTQUFFLENBQUEsZ0JBQVAsR0FBMEIsU0FBQyxVQUFEO0FBQ3RCLE1BQUE7RUFBQSxDQUFBLEdBQUksSUFBSTtFQUNSLENBQUMsQ0FBQyxLQUFGLEdBQVUsSUFBQyxDQUFBO0VBQ1gsQ0FBQyxDQUFDLFVBQUYsR0FBZSxJQUFDLENBQUE7RUFDaEIsQ0FBQyxDQUFDLE1BQUYsQ0FBUyxDQUFULEVBQVcsVUFBWDtFQUNBLElBQUMsQ0FBQSxPQUFELENBQUE7U0FDQTtBQU5zQjs7QUFRMUIsT0FBTyxDQUFDLFNBQVIsR0FBb0I7O0FBQ3BCLE9BQU8sQ0FBQyxpQkFBUixHQUE0Qjs7OztBQ3JINUIsSUFBQTs7QUFBQSxLQUFLLENBQUEsU0FBRSxDQUFBLEtBQVAsR0FBZSxTQUFDLE9BQUQ7O0lBQUMsVUFBVTs7U0FDeEIsTUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFiLENBQXNCLElBQXRCLEVBQXlCLE9BQXpCO0FBRGE7O0FBU2YsS0FBSyxDQUFBLFNBQUUsQ0FBQSxRQUFBLENBQVAsR0FBZ0IsU0FBQTtTQUNkLE1BQU0sQ0FBQyxLQUFLLENBQUMsVUFBYixDQUF3QixJQUF4QjtBQURjOztBQVNoQixLQUFLLENBQUEsU0FBRSxDQUFBLEdBQVAsR0FBYSxTQUFBO1NBQ1gsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFiLENBQWlCLElBQWpCO0FBRFc7O0FBUWIsS0FBSyxDQUFBLFNBQUUsQ0FBQSxLQUFQLEdBQWUsU0FBQTtTQUNiLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBYixDQUFtQixJQUFuQjtBQURhOztBQVFUO0FBR0osTUFBQTs7RUFBQSxRQUFBLEdBQVc7O0VBR0UscUJBQUE7QUFDWCxRQUFBO0lBQUEsSUFBQSxHQUFPO0lBQ1AsTUFBTSxDQUFDLFFBQVAsR0FBa0IsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLEdBQUQ7ZUFDaEIsS0FBQyxDQUFBLFFBQUQsQ0FBQTtNQURnQjtJQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7RUFGUDs7d0JBTWIsUUFBQSxHQUFVLFNBQUMsS0FBRCxFQUFRLE9BQVI7O01BQVEsVUFBVTs7V0FDMUIsSUFBQyxDQUFBLFNBQUQsQ0FBVyxLQUFYLEVBQWtCLE9BQWxCO0VBRFE7O3dCQUlWLFVBQUEsR0FBWSxTQUFDLEtBQUQ7V0FDVixJQUFDLENBQUEsWUFBRCxDQUFjLEtBQWQ7RUFEVTs7d0JBSVosR0FBQSxHQUFLLFNBQUMsS0FBRDtJQUNILEtBQUssQ0FBQyxLQUFOLEdBQ0U7TUFBQSxRQUFBLEVBQVUsT0FBVjs7V0FDRjtFQUhHOzt3QkFJTCxLQUFBLEdBQU8sU0FBQyxLQUFEO0lBQ0wsS0FBSyxDQUFDLEtBQU4sR0FDRTtNQUFBLFFBQUEsRUFBVSxVQUFWOztXQUNGO0VBSEs7O3dCQVVQLE1BQUEsR0FBUSxTQUFBO1dBQ047RUFETTs7d0JBSVIsUUFBQSxHQUFVLFNBQUE7QUFDUixRQUFBO0lBQUEsSUFBQSxHQUFPO1dBQ1AsQ0FBQyxDQUFDLElBQUYsQ0FBTyxRQUFQLEVBQWlCLFNBQUMsR0FBRCxFQUFNLEtBQU47YUFDZixJQUFJLENBQUMsYUFBTCxDQUFtQixHQUFuQjtJQURlLENBQWpCO0VBRlE7O3dCQU1WLGFBQUEsR0FBZSxTQUFDLEdBQUQ7QUFDYixRQUFBO0lBQUEsS0FBQSxHQUFRLEdBQUcsQ0FBQztJQUVaLElBQUcscUJBQUg7TUFDRSxRQUFBLEdBQWMsdUJBQUgsR0FBeUIsSUFBQyxDQUFBLFlBQUQsQ0FBYyxLQUFkLENBQUEsR0FDbEMsR0FBRyxDQUFDLFdBREssR0FDWSxJQUFDLENBQUEsWUFBRCxDQUFjLEtBQWQ7TUFDdkIsS0FBSyxDQUFDLEtBQU4sR0FBYztNQUNkLEtBQUssQ0FBQyxLQUFOLEdBQ0U7UUFBQSxrQkFBQSxFQUFvQixRQUFwQjtRQUxKOztJQU1BLElBQUcsc0JBQUg7TUFDRSxTQUFBLEdBQWUsd0JBQUgsR0FBMEIsSUFBQyxDQUFBLGFBQUQsQ0FBZSxLQUFmLENBQUEsR0FDcEMsR0FBRyxDQUFDLFlBRE0sR0FDWSxJQUFDLENBQUEsYUFBRCxDQUFlLEtBQWY7TUFDeEIsS0FBSyxDQUFDLE1BQU4sR0FBZTtNQUNmLEtBQUssQ0FBQyxLQUFOLEdBQ0U7UUFBQSxrQkFBQSxFQUFvQixRQUFwQjtRQUxKOztBQU9BLFlBQU8sR0FBRyxDQUFDLE1BQVg7QUFBQSxXQUNPLE1BRFA7UUFFSSxLQUFLLENBQUMsQ0FBTixHQUFVLElBQUMsQ0FBQSxZQUFELENBQWMsR0FBZCxFQUFtQixDQUFuQjtBQURQO0FBRFAsV0FHTyxPQUhQO1FBSUksSUFBQSxHQUFPLElBQUMsQ0FBQSxZQUFELENBQWMsS0FBZCxDQUFBLEdBQXVCLEtBQUssQ0FBQztRQUNwQyxLQUFLLENBQUMsQ0FBTixHQUFVLElBQUMsQ0FBQSxZQUFELENBQWMsR0FBZCxFQUFtQixJQUFuQjtBQUZQO0FBSFAsV0FNTyxRQU5QO1FBT0ksS0FBSyxDQUFDLE9BQU4sQ0FBQTtRQUNBLEtBQUssQ0FBQyxDQUFOLEdBQVUsSUFBQyxDQUFBLFlBQUQsQ0FBYyxHQUFkLEVBQW1CLEtBQUssQ0FBQyxDQUF6QjtBQUZQO0FBTlAsV0FTTyxRQVRQO1FBVUksS0FBSyxDQUFDLE9BQU4sQ0FBQTtRQUNBLEtBQUssQ0FBQyxDQUFOLEdBQVUsSUFBQyxDQUFBLFlBQUQsQ0FBYyxHQUFkLEVBQW1CLEtBQUssQ0FBQyxDQUF6QjtBQVhkO0FBWUEsWUFBTyxHQUFHLENBQUMsTUFBWDtBQUFBLFdBQ08sUUFEUDtRQUVJLElBQUEsR0FBTyxJQUFDLENBQUEsYUFBRCxDQUFlLEtBQWYsQ0FBQSxHQUF3QixLQUFLLENBQUM7ZUFDckMsS0FBSyxDQUFDLENBQU4sR0FBVSxJQUFDLENBQUEsWUFBRCxDQUFjLEdBQWQsRUFBbUIsSUFBbkI7QUFIZCxXQUlPLEtBSlA7ZUFLSSxLQUFLLENBQUMsQ0FBTixHQUFVLElBQUMsQ0FBQSxZQUFELENBQWMsR0FBZCxFQUFtQixDQUFuQjtBQUxkLFdBTU8sUUFOUDtRQU9JLEtBQUssQ0FBQyxPQUFOLENBQUE7ZUFDQSxLQUFLLENBQUMsQ0FBTixHQUFVLElBQUMsQ0FBQSxZQUFELENBQWMsR0FBZCxFQUFtQixLQUFLLENBQUMsQ0FBekI7QUFSZCxXQVNPLFFBVFA7UUFVSSxLQUFLLENBQUMsT0FBTixDQUFBO2VBQ0EsS0FBSyxDQUFDLENBQU4sR0FBVSxJQUFDLENBQUEsWUFBRCxDQUFjLEdBQWQsRUFBbUIsS0FBSyxDQUFDLENBQXpCO0FBWGQ7RUE1QmE7O3dCQTBDZixZQUFBLEdBQWMsU0FBQyxHQUFELEVBQU0sQ0FBTjtXQUNaLENBQUEsR0FBTyxtQkFBSCxHQUFxQixDQUFBLEdBQUksR0FBRyxDQUFDLE9BQTdCLEdBQTBDO0VBRGxDOzt3QkFFZCxZQUFBLEdBQWMsU0FBQyxHQUFELEVBQU0sQ0FBTjtXQUNaLENBQUEsR0FBTyxtQkFBSCxHQUFxQixDQUFBLEdBQUksR0FBRyxDQUFDLE9BQTdCLEdBQTBDO0VBRGxDOzt3QkFJZCxZQUFBLEdBQWMsU0FBQyxLQUFEO0lBQ1osSUFBRyx3QkFBSDthQUEwQixLQUFLLENBQUMsVUFBVSxDQUFDLE1BQTNDO0tBQUEsTUFBQTthQUFzRCxNQUFNLENBQUMsV0FBN0Q7O0VBRFk7O3dCQUVkLGFBQUEsR0FBZSxTQUFDLEtBQUQ7SUFDYixJQUFHLHdCQUFIO2FBQTBCLEtBQUssQ0FBQyxVQUFVLENBQUMsT0FBM0M7S0FBQSxNQUFBO2FBQXVELE1BQU0sQ0FBQyxZQUE5RDs7RUFEYTs7d0JBSWYsU0FBQSxHQUFXLFNBQUMsS0FBRCxFQUFRLE9BQVI7QUFDVCxRQUFBOztNQURpQixVQUFVOztJQUMzQixHQUFBLEdBQU0sQ0FBQyxDQUFDLE1BQUYsQ0FBUyxPQUFULEVBQWtCO01BQUEsV0FBQSxFQUFhLEtBQWI7S0FBbEI7SUFDTixRQUFRLENBQUMsSUFBVCxDQUFjLEdBQWQ7SUFFQSxJQUFBLEdBQU87SUFDUCxLQUFLLENBQUMsV0FBTixDQUFrQixTQUFBO2FBQ2hCLElBQUksQ0FBQyxhQUFMLENBQW1CLEdBQW5CLEVBQXdCLElBQXhCO0lBRGdCLENBQWxCO1dBRUE7RUFQUzs7d0JBVVgsWUFBQSxHQUFjLFNBQUMsS0FBRDtBQUNaLFFBQUE7SUFBQSxNQUFBLEdBQVMsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxRQUFaLEVBQXNCO01BQUMsV0FBQSxFQUFhLEtBQWQ7S0FBdEI7SUFFVCxJQUFpQixjQUFqQjtBQUFBLGFBQU8sTUFBUDs7SUFDQSxJQUFHLDBCQUFBLElBQXFCLDJCQUF4QjtNQUNFLE1BQU0sQ0FBQyxLQUFQLEdBQ0U7UUFBQSxrQkFBQSxFQUFvQixTQUFwQjtRQUZKOztJQUdBLFFBQUEsR0FBVyxDQUFDLENBQUMsT0FBRixDQUFVLFFBQVYsRUFBb0IsTUFBcEI7V0FDWDtFQVJZOzs7Ozs7QUFXaEIsTUFBTSxDQUFDLEtBQVAsR0FBZSxJQUFJIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsImNsYXNzIFRleHRMYXllciBleHRlbmRzIExheWVyXG5cdFx0XG5cdGNvbnN0cnVjdG9yOiAob3B0aW9ucz17fSkgLT5cblx0XHRAZG9BdXRvU2l6ZSA9IGZhbHNlXG5cdFx0QGRvQXV0b1NpemVIZWlnaHQgPSBmYWxzZVxuXHRcdG9wdGlvbnMuYmFja2dyb3VuZENvbG9yID89IGlmIG9wdGlvbnMuc2V0dXAgdGhlbiBcImhzbGEoNjAsIDkwJSwgNDclLCAuNClcIiBlbHNlIFwidHJhbnNwYXJlbnRcIlxuXHRcdG9wdGlvbnMuY29sb3IgPz0gXCJyZWRcIlxuXHRcdG9wdGlvbnMubGluZUhlaWdodCA/PSAxLjI1XG5cdFx0b3B0aW9ucy5mb250RmFtaWx5ID89IFwiSGVsdmV0aWNhXCJcblx0XHRvcHRpb25zLmZvbnRTaXplID89IDIwXG5cdFx0b3B0aW9ucy50ZXh0ID89IFwiVXNlIGxheWVyLnRleHQgdG8gYWRkIHRleHRcIlxuXHRcdHN1cGVyIG9wdGlvbnNcblx0XHRAc3R5bGUud2hpdGVTcGFjZSA9IFwicHJlLWxpbmVcIiAjIGFsbG93IFxcbiBpbiAudGV4dFxuXHRcdEBzdHlsZS5vdXRsaW5lID0gXCJub25lXCIgIyBubyBib3JkZXIgd2hlbiBzZWxlY3RlZFxuXHRcdFxuXHRzZXRTdHlsZTogKHByb3BlcnR5LCB2YWx1ZSwgcHhTdWZmaXggPSBmYWxzZSkgLT5cblx0XHRAc3R5bGVbcHJvcGVydHldID0gaWYgcHhTdWZmaXggdGhlbiB2YWx1ZStcInB4XCIgZWxzZSB2YWx1ZVxuXHRcdEBlbWl0KFwiY2hhbmdlOiN7cHJvcGVydHl9XCIsIHZhbHVlKVxuXHRcdGlmIEBkb0F1dG9TaXplIHRoZW4gQGNhbGNTaXplKClcblx0XHRcblx0Y2FsY1NpemU6IC0+XG5cdFx0c2l6ZUFmZmVjdGluZ1N0eWxlcyA9XG5cdFx0XHRsaW5lSGVpZ2h0OiBAc3R5bGVbXCJsaW5lLWhlaWdodFwiXVxuXHRcdFx0Zm9udFNpemU6IEBzdHlsZVtcImZvbnQtc2l6ZVwiXVxuXHRcdFx0Zm9udFdlaWdodDogQHN0eWxlW1wiZm9udC13ZWlnaHRcIl1cblx0XHRcdHBhZGRpbmdUb3A6IEBzdHlsZVtcInBhZGRpbmctdG9wXCJdXG5cdFx0XHRwYWRkaW5nUmlnaHQ6IEBzdHlsZVtcInBhZGRpbmctcmlnaHRcIl1cblx0XHRcdHBhZGRpbmdCb3R0b206IEBzdHlsZVtcInBhZGRpbmctYm90dG9tXCJdXG5cdFx0XHRwYWRkaW5nTGVmdDogQHN0eWxlW1wicGFkZGluZy1sZWZ0XCJdXG5cdFx0XHR0ZXh0VHJhbnNmb3JtOiBAc3R5bGVbXCJ0ZXh0LXRyYW5zZm9ybVwiXVxuXHRcdFx0Ym9yZGVyV2lkdGg6IEBzdHlsZVtcImJvcmRlci13aWR0aFwiXVxuXHRcdFx0bGV0dGVyU3BhY2luZzogQHN0eWxlW1wibGV0dGVyLXNwYWNpbmdcIl1cblx0XHRcdGZvbnRGYW1pbHk6IEBzdHlsZVtcImZvbnQtZmFtaWx5XCJdXG5cdFx0XHRmb250U3R5bGU6IEBzdHlsZVtcImZvbnQtc3R5bGVcIl1cblx0XHRcdGZvbnRWYXJpYW50OiBAc3R5bGVbXCJmb250LXZhcmlhbnRcIl1cblx0XHRjb25zdHJhaW50cyA9IHt9XG5cdFx0aWYgQGRvQXV0b1NpemVIZWlnaHQgdGhlbiBjb25zdHJhaW50cy53aWR0aCA9IEB3aWR0aFxuXHRcdHNpemUgPSBVdGlscy50ZXh0U2l6ZSBAdGV4dCwgc2l6ZUFmZmVjdGluZ1N0eWxlcywgY29uc3RyYWludHNcblx0XHRpZiBAc3R5bGUudGV4dEFsaWduIGlzIFwicmlnaHRcIlxuXHRcdFx0QHdpZHRoID0gc2l6ZS53aWR0aFxuXHRcdFx0QHggPSBAeC1Ad2lkdGhcblx0XHRlbHNlXG5cdFx0XHRAd2lkdGggPSBzaXplLndpZHRoXG5cdFx0QGhlaWdodCA9IHNpemUuaGVpZ2h0XG5cblx0QGRlZmluZSBcImF1dG9TaXplXCIsXG5cdFx0Z2V0OiAtPiBAZG9BdXRvU2l6ZVxuXHRcdHNldDogKHZhbHVlKSAtPiBcblx0XHRcdEBkb0F1dG9TaXplID0gdmFsdWVcblx0XHRcdGlmIEBkb0F1dG9TaXplIHRoZW4gQGNhbGNTaXplKClcblx0QGRlZmluZSBcImF1dG9TaXplSGVpZ2h0XCIsXG5cdFx0c2V0OiAodmFsdWUpIC0+IFxuXHRcdFx0QGRvQXV0b1NpemUgPSB2YWx1ZVxuXHRcdFx0QGRvQXV0b1NpemVIZWlnaHQgPSB2YWx1ZVxuXHRcdFx0aWYgQGRvQXV0b1NpemUgdGhlbiBAY2FsY1NpemUoKVxuXHRAZGVmaW5lIFwiY29udGVudEVkaXRhYmxlXCIsXG5cdFx0c2V0OiAoYm9vbGVhbikgLT5cblx0XHRcdEBfZWxlbWVudC5jb250ZW50RWRpdGFibGUgPSBib29sZWFuXG5cdFx0XHRAaWdub3JlRXZlbnRzID0gIWJvb2xlYW5cblx0XHRcdEBvbiBcImlucHV0XCIsIC0+IEBjYWxjU2l6ZSgpIGlmIEBkb0F1dG9TaXplXG5cdEBkZWZpbmUgXCJ0ZXh0XCIsXG5cdFx0Z2V0OiAtPiBAX2VsZW1lbnQudGV4dENvbnRlbnRcblx0XHRzZXQ6ICh2YWx1ZSkgLT5cblx0XHRcdEBfZWxlbWVudC50ZXh0Q29udGVudCA9IHZhbHVlXG5cdFx0XHRAZW1pdChcImNoYW5nZTp0ZXh0XCIsIHZhbHVlKVxuXHRcdFx0aWYgQGRvQXV0b1NpemUgdGhlbiBAY2FsY1NpemUoKVxuXHRAZGVmaW5lIFwiZm9udEZhbWlseVwiLCBcblx0XHRnZXQ6IC0+IEBzdHlsZS5mb250RmFtaWx5XG5cdFx0c2V0OiAodmFsdWUpIC0+IEBzZXRTdHlsZShcImZvbnRGYW1pbHlcIiwgdmFsdWUpXG5cdEBkZWZpbmUgXCJmb250U2l6ZVwiLCBcblx0XHRnZXQ6IC0+IEBzdHlsZS5mb250U2l6ZS5yZXBsYWNlKFwicHhcIixcIlwiKVxuXHRcdHNldDogKHZhbHVlKSAtPiBAc2V0U3R5bGUoXCJmb250U2l6ZVwiLCB2YWx1ZSwgdHJ1ZSlcblx0QGRlZmluZSBcImxpbmVIZWlnaHRcIiwgXG5cdFx0Z2V0OiAtPiBAc3R5bGUubGluZUhlaWdodCBcblx0XHRzZXQ6ICh2YWx1ZSkgLT4gQHNldFN0eWxlKFwibGluZUhlaWdodFwiLCB2YWx1ZSlcblx0QGRlZmluZSBcImZvbnRXZWlnaHRcIiwgXG5cdFx0Z2V0OiAtPiBAc3R5bGUuZm9udFdlaWdodCBcblx0XHRzZXQ6ICh2YWx1ZSkgLT4gQHNldFN0eWxlKFwiZm9udFdlaWdodFwiLCB2YWx1ZSlcblx0QGRlZmluZSBcImZvbnRTdHlsZVwiLCBcblx0XHRnZXQ6IC0+IEBzdHlsZS5mb250U3R5bGVcblx0XHRzZXQ6ICh2YWx1ZSkgLT4gQHNldFN0eWxlKFwiZm9udFN0eWxlXCIsIHZhbHVlKVxuXHRAZGVmaW5lIFwiZm9udFZhcmlhbnRcIiwgXG5cdFx0Z2V0OiAtPiBAc3R5bGUuZm9udFZhcmlhbnRcblx0XHRzZXQ6ICh2YWx1ZSkgLT4gQHNldFN0eWxlKFwiZm9udFZhcmlhbnRcIiwgdmFsdWUpXG5cdEBkZWZpbmUgXCJwYWRkaW5nXCIsXG5cdFx0c2V0OiAodmFsdWUpIC0+IFxuXHRcdFx0QHNldFN0eWxlKFwicGFkZGluZ1RvcFwiLCB2YWx1ZSwgdHJ1ZSlcblx0XHRcdEBzZXRTdHlsZShcInBhZGRpbmdSaWdodFwiLCB2YWx1ZSwgdHJ1ZSlcblx0XHRcdEBzZXRTdHlsZShcInBhZGRpbmdCb3R0b21cIiwgdmFsdWUsIHRydWUpXG5cdFx0XHRAc2V0U3R5bGUoXCJwYWRkaW5nTGVmdFwiLCB2YWx1ZSwgdHJ1ZSlcblx0QGRlZmluZSBcInBhZGRpbmdUb3BcIiwgXG5cdFx0Z2V0OiAtPiBAc3R5bGUucGFkZGluZ1RvcC5yZXBsYWNlKFwicHhcIixcIlwiKVxuXHRcdHNldDogKHZhbHVlKSAtPiBAc2V0U3R5bGUoXCJwYWRkaW5nVG9wXCIsIHZhbHVlLCB0cnVlKVxuXHRAZGVmaW5lIFwicGFkZGluZ1JpZ2h0XCIsIFxuXHRcdGdldDogLT4gQHN0eWxlLnBhZGRpbmdSaWdodC5yZXBsYWNlKFwicHhcIixcIlwiKVxuXHRcdHNldDogKHZhbHVlKSAtPiBAc2V0U3R5bGUoXCJwYWRkaW5nUmlnaHRcIiwgdmFsdWUsIHRydWUpXG5cdEBkZWZpbmUgXCJwYWRkaW5nQm90dG9tXCIsIFxuXHRcdGdldDogLT4gQHN0eWxlLnBhZGRpbmdCb3R0b20ucmVwbGFjZShcInB4XCIsXCJcIilcblx0XHRzZXQ6ICh2YWx1ZSkgLT4gQHNldFN0eWxlKFwicGFkZGluZ0JvdHRvbVwiLCB2YWx1ZSwgdHJ1ZSlcblx0QGRlZmluZSBcInBhZGRpbmdMZWZ0XCIsXG5cdFx0Z2V0OiAtPiBAc3R5bGUucGFkZGluZ0xlZnQucmVwbGFjZShcInB4XCIsXCJcIilcblx0XHRzZXQ6ICh2YWx1ZSkgLT4gQHNldFN0eWxlKFwicGFkZGluZ0xlZnRcIiwgdmFsdWUsIHRydWUpXG5cdEBkZWZpbmUgXCJ0ZXh0QWxpZ25cIixcblx0XHRzZXQ6ICh2YWx1ZSkgLT4gQHNldFN0eWxlKFwidGV4dEFsaWduXCIsIHZhbHVlKVxuXHRAZGVmaW5lIFwidGV4dFRyYW5zZm9ybVwiLCBcblx0XHRnZXQ6IC0+IEBzdHlsZS50ZXh0VHJhbnNmb3JtIFxuXHRcdHNldDogKHZhbHVlKSAtPiBAc2V0U3R5bGUoXCJ0ZXh0VHJhbnNmb3JtXCIsIHZhbHVlKVxuXHRAZGVmaW5lIFwibGV0dGVyU3BhY2luZ1wiLCBcblx0XHRnZXQ6IC0+IEBzdHlsZS5sZXR0ZXJTcGFjaW5nLnJlcGxhY2UoXCJweFwiLFwiXCIpXG5cdFx0c2V0OiAodmFsdWUpIC0+IEBzZXRTdHlsZShcImxldHRlclNwYWNpbmdcIiwgdmFsdWUsIHRydWUpXG5cdEBkZWZpbmUgXCJsZW5ndGhcIiwgXG5cdFx0Z2V0OiAtPiBAdGV4dC5sZW5ndGhcblxuY29udmVydFRvVGV4dExheWVyID0gKGxheWVyKSAtPlxuXHR0ID0gbmV3IFRleHRMYXllclxuXHRcdG5hbWU6IGxheWVyLm5hbWVcblx0XHRmcmFtZTogbGF5ZXIuZnJhbWVcblx0XHRwYXJlbnQ6IGxheWVyLnBhcmVudFxuXHRcblx0Y3NzT2JqID0ge31cblx0Y3NzID0gbGF5ZXIuX2luZm8ubWV0YWRhdGEuY3NzXG5cdGNzcy5mb3JFYWNoIChydWxlKSAtPlxuXHRcdHJldHVybiBpZiBfLmluY2x1ZGVzIHJ1bGUsICcvKidcblx0XHRhcnIgPSBydWxlLnNwbGl0KCc6ICcpXG5cdFx0Y3NzT2JqW2FyclswXV0gPSBhcnJbMV0ucmVwbGFjZSgnOycsJycpXG5cdHQuc3R5bGUgPSBjc3NPYmpcblx0XG5cdGltcG9ydFBhdGggPSBsYXllci5fX2ZyYW1lckltcG9ydGVkRnJvbVBhdGhcblx0aWYgXy5pbmNsdWRlcyBpbXBvcnRQYXRoLCAnQDJ4J1xuXHRcdHQuZm9udFNpemUgKj0gMlxuXHRcdHQubGluZUhlaWdodCA9IChwYXJzZUludCh0LmxpbmVIZWlnaHQpKjIpKydweCdcblx0XHR0LmxldHRlclNwYWNpbmcgKj0gMlxuXHRcdFx0XHRcdFxuXHR0LnkgLT0gKHBhcnNlSW50KHQubGluZUhlaWdodCktdC5mb250U2l6ZSkvMiAjIGNvbXBlbnNhdGUgZm9yIGhvdyBDU1MgaGFuZGxlcyBsaW5lIGhlaWdodFxuXHR0LnkgLT0gdC5mb250U2l6ZSAqIDAuMSAjIHNrZXRjaCBwYWRkaW5nXG5cdHQueCAtPSB0LmZvbnRTaXplICogMC4wOCAjIHNrZXRjaCBwYWRkaW5nXG5cdHQud2lkdGggKz0gdC5mb250U2l6ZSAqIDAuNSAjIHNrZXRjaCBwYWRkaW5nXG5cblx0dC50ZXh0ID0gbGF5ZXIuX2luZm8ubWV0YWRhdGEuc3RyaW5nXG5cdGxheWVyLmRlc3Ryb3koKVxuXHRyZXR1cm4gdFxuXG5MYXllcjo6Y29udmVydFRvVGV4dExheWVyID0gLT4gY29udmVydFRvVGV4dExheWVyKEApXG5cbmNvbnZlcnRUZXh0TGF5ZXJzID0gKG9iaikgLT5cblx0Zm9yIHByb3AsbGF5ZXIgb2Ygb2JqXG5cdFx0aWYgbGF5ZXIuX2luZm8ua2luZCBpcyBcInRleHRcIlxuXHRcdFx0b2JqW3Byb3BdID0gY29udmVydFRvVGV4dExheWVyKGxheWVyKVxuXG4jIEJhY2t3YXJkcyBjb21wYWJpbGl0eS4gUmVwbGFjZWQgYnkgY29udmVydFRvVGV4dExheWVyKClcbkxheWVyOjpmcmFtZUFzVGV4dExheWVyID0gKHByb3BlcnRpZXMpIC0+XG4gICAgdCA9IG5ldyBUZXh0TGF5ZXJcbiAgICB0LmZyYW1lID0gQGZyYW1lXG4gICAgdC5zdXBlckxheWVyID0gQHN1cGVyTGF5ZXJcbiAgICBfLmV4dGVuZCB0LHByb3BlcnRpZXNcbiAgICBAZGVzdHJveSgpXG4gICAgdFxuXG5leHBvcnRzLlRleHRMYXllciA9IFRleHRMYXllclxuZXhwb3J0cy5jb252ZXJ0VGV4dExheWVycyA9IGNvbnZlcnRUZXh0TGF5ZXJzXG4iLCIjIEZyYW1lci5qcyBMYXllciBleHRlbnNpb25zXG4jIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4jXG4jIElmIHlvdSB1c2UgdGhlIEZyYW1lci5qcyBwcm90b3R5cGluZyBmcmFtZXdvcmsgYW5kIHlvdSB3YW50IHRvIGJ1aWxkIFwicmVzcG9uc2l2ZVwiIHByb3RvdHlwZXMsIHRoaXMgc2V0IG9mIGV4dGVuc2lvbnMgdG8gdGhlIEZyYW1lci5qcyBMYXllciBvYmplY3QgY2FuIGJlIG9mIGFzc2lzdGFuY2UuXG4jIFxuIyAjIyMjTGF5ZXIuZmx1aWQoKVxuIyBJbml0aWFsaXplIGZsdWlkIGxheW91dCBmb3IgYSBsYXllci4gWW91IGNhbiBtYWtlIGFueSBsYXllcidzIHdpZHRoIGFuZCBoZWlnaHQgZHluYW1pYyBvciBzZXQgaXRzIHBvc2l0aW9uIGFic29sdXRlbHkgcmVsYXRpdmUgdG8gdGhlIGNvbnRhaW5pbmcgZWxlbWVudC5cbiNcbiMgTGF5ZXJzIGFyZSByZXNpemVkIGFuZCByZXBvc2l0aW9uZWQgYXMgc29vbiBhcyB0aGV5IGFyZSBpbml0aWFsaXplZCBhbmQgd2hlbmV2ZXIgdGhlIGJyb3dzZXIgd2luZG93IGlzIHJlc2l6ZWQuXG4jXG4jIFlvdSBtaWdodCB3YW50IHRvIG1ha2UgYW4gaW1hZ2Ugc3RyZXRjaCBhbmQgZmlsbCB0aGUgZW50aXJlIGJyb3dzZXI6XG4jIGBgYFxuIyBteUxheWVyLmZsdWlkXG4jICAgYXV0b1dpZHRoOiB0cnVlXG4jICAgYXV0b0hlaWdodDogdHJ1ZVxuIyBgYGBcbiNcbiMgT3IgbWF5YmUgeW91IHdhbnQgdG8gcGluIGEgbGF5ZXIgdG8gdGhlIGxvd2VyIGxlZnQgb2YgaXRzIHBhcmVudCB3aXRoIDEwcHggb2YgcGFkZGluZzpcbiMgYGBgXG4jIG15TGF5ZXIuZmx1aWRcbiMgICB4QWxpZ246ICdsZWZ0J1xuIyAgIHhPZmZzZXQ6IDEwXG4jICAgeUFsaWduOiAnYm90dG9tJ1xuIyAgIHlPZmZzZXQ6IC0xMFxuIyBgYGBcbiNcbiMgSGVyZSBhcmUgYWxsIHRoZSBhdmFpbGFibGUgb3B0aW9uczpcbiNcbiMgYGBgXG4jICMgRXhwYW5kIHdpZHRoIG9yIGhlaWdodCB0byBmaWxsIGNvbnRhaW5pbmcgZWxlbWVudFxuIyBhdXRvV2lkdGg6IEJvb2xlYW5cbiMgYXV0b0hlaWdodDogQm9vbGVhbiBcbiMgIyBBZGQgb3Igc3VidHJhY3QgZnJvbSB0aGUgY2FsY3VsYXRlZCB3aWR0aC9oZWlnaHRcbiMgaGVpZ2h0T2Zmc2V0OiBOdW1iZXJcbiMgd2lkdGhPZmZzZXQ6IE51bWJlclxuIyAjIEFsaWdubWVudCB3aXRoaW4gY29udGFpbmluZyBlbGVtZW50XG4jIHhBbGlnbjogJ2xlZnQnIHwgJ3JpZ2h0JyB8ICdjZW50ZXInIG9yICdtaWRkbGUnXG4jIHlBbGlnbjogJ2JvdHRvbScgfCAndG9wJyB8ICdjZW50ZXInIG9yICdtaWRkbGUnXG4jICMgUG9zaXRpb24gcmVsYXRpdmUgdG8gYWxpZ25tZW50XG4jIHhPZmZzZXQ6IE51bWJlclxuIyB5T2Zmc2V0OiBOdW1iZXJcbiMgYGBgXG5MYXllcjo6Zmx1aWQgPSAob3B0aW9ucyA9IHt9KSAtPlxuICBGcmFtZXIuRmx1aWQucmVnaXN0ZXIgQCwgb3B0aW9uc1xuXG4jICMjIyNMYXllci5zdGF0aWMoKVxuIyBSZW1vdmUgYSBsYXllcidzIGZsdWlkIHBvc2l0aW9uaW5nLiBXaGF0ZXZlciBvcHRpb25zIHlvdSBzcGVjaWZpZWQgd2hlbiB5b3UgbWFkZSB0aGUgbGF5ZXIgZmx1aWQgaW5pdGlhbGx5IHdpbGwgbm8gbG9uZ2VyIGFwcGx5LiBUaGlzIHdvbid0IHJlc2V0IGl0IHRvIGl0cyBpbml0aWFsIHBvc2l0aW9uLCBob3dldmVyLlxuI1xuIyBgYGBcbiMgbXlMYXllci5zdGF0aWMoKVxuIyBgYGBcbkxheWVyOjpzdGF0aWMgPSAtPlxuICBGcmFtZXIuRmx1aWQudW5yZWdpc3RlciBAXG5cbiMgIyMjI0xheWVyLmZpeCgpXG4jIEFuIGVhc3kgd2F5IHRvIHNldCBhIGxheWVyIHRvIGBwb3NpdGlvbjogZml4ZWRgLiBJZiB5b3Ugd2FudCB0byBwaW4gYSBzdGF0aWMgaGVhZGVyIG9yIHNpZGViYXIgaW4geW91ciBhcHAncyBwcm90b3R5cGUsIHRoaXMgY2FuIGJlIHlvdXIgZnJpZW5kLlxuI1xuIyBgYGBcbiMgbXlMYXllci5maXgoKVxuIyBgYGBcbkxheWVyOjpmaXggPSAtPlxuICBGcmFtZXIuRmx1aWQuZml4IEBcblxuIyAjIyMjTGF5ZXIudW5maXgoKVxuIyBBbiBlYXN5IHdheSB0byByZXNldCBzYWlkIGxheWVyIHRvIEZyYW1lcidzIGRlZmF1bHQgcG9zaXRpb25pbmcuXG4jIGBgYFxuIyBteUxheWVyLnVuZml4KClcbiMgYGBgXG5MYXllcjo6dW5maXggPSAtPlxuICBGcmFtZXIuRmx1aWQudW5maXggQFxuXG5cbiMgRmx1aWRGcmFtZXIgaW1wbGVtZW50YXRpb25cbiMgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiMgXG4jIElmIHlvdSBpbmNsdWRlIGZsdWlkLWZyYW1lci5qcyBpbiB5b3VyIHByb3RvdHlwZSdzIGluZGV4Lmh0bWwgZmlsZSwgaXQnbGwgYXV0b21hdGljYWxseSBjcmVhdGUgYW4gaW5zdGFuY2Ugb2YgdGhpcyBjbGFzcyBhbmQgYWRkIGFsbCBvZiB0aGUgc2hvcnRoYW5kIG1ldGhvZHMgYWJvdmUgdG8gdGhlIEZyYW1lci5qcyBMYXllciBvYmplY3QncyBwcm90b3R5cGUgY2hhaW4uIFNvIHVubGVzcyB5b3UncmUgY3VyaW91cywgdGhlIHJlc3QgaXMgYmFzaWNhbGx5ICBpbXBsZW1lbnRhdGlvbiBkZXRhaWxzLlxuY2xhc3MgRmx1aWRGcmFtZXJcblxuICAjIEFycmF5IHRoYXQgY29udGFpbnMgYWxsIG9mIHRoZSBmbHVpZCBMYXllcnMuXG4gIHJlZ2lzdHJ5ID0gW11cblxuICAjIExpc3RlbiBmb3IgYHdpbmRvdy5vbnJlc2l6ZWAgYW5kIHJlc3BvbmQgYnkgYWRqdXN0aW5nIHBvc2l0aW9uIGFuZCBkaW1lbnNpb25zIG9mIGZsdWlkIExheWVycy5cbiAgY29uc3RydWN0b3I6IC0+XG4gICAgc2VsZiA9IEBcbiAgICB3aW5kb3cub25yZXNpemUgPSAoZXZ0KSA9PlxuICAgICAgQF9yZXNwb25kKClcblxuICAjIFB1YmxpYyBtZXRob2QgdG8gbWFrZSBhIExheWVyIGZsdWlkLiBDYWxsZWQgZGlyZWN0bHkgbGlrZSBgRnJhbWVyLkZsdWlkLnJlZ2lzdGVyKG15TGF5ZXIpYCBidXQgd2h5IGJvdGhlciB3aGVuIHlvdSBjYW4ganVzdCBiZSBhbGwgYG15TGF5ZXIuZmx1aWQoKWA/IFxuICByZWdpc3RlcjogKGxheWVyLCBvcHRpb25zID0ge30pIC0+XG4gICAgQF9hZGRMYXllciBsYXllciwgb3B0aW9uc1xuXG4gICMgU2FtZXNpZXMgdG8gc3RvcCByZXNpemluZy9yZXBvc2l0aW9uaW5nLlxuICB1bnJlZ2lzdGVyOiAobGF5ZXIpIC0+XG4gICAgQF9yZW1vdmVMYXllciBsYXllclxuICBcbiAgIyBBbmQgdGhlc2UgYXJlIGhvdyB3ZSBjaGFuZ2UgYSBMYXllciBmcm9tIGFic29sdXRlIHRvIGZpeGVkIHBvc2l0aW9uaW5nIGFuZCBiYWNrIGFnYWluLlxuICBmaXg6IChsYXllcikgLT5cbiAgICBsYXllci5zdHlsZSA9XG4gICAgICBwb3NpdGlvbjogJ2ZpeGVkJ1xuICAgIGxheWVyXG4gIHVuZml4OiAobGF5ZXIpIC0+XG4gICAgbGF5ZXIuc3R5bGUgPVxuICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZSdcbiAgICBsYXllclxuXG4gICMgIyMjI0ZyYW1lci5GbHVpZC5sYXllcnMoKVxuICAjIFJldHVybnMgYWxsIG9mIHRoZSBMYXllcnMgdGhhdCBhcmUgcmVnaXN0ZXJlZC4gQ291bGQgaGVscCB3aXRoIGRlYnVnZ2luZz8gTWF5YmU/XG4gICMgYGBgXG4gICMgRnJhbWVyLkZsdWlkLmxheWVycygpICMgUmV0dXJucyBhbiBhcnJheSBvZiBMYXllcnNcbiAgIyBgYGBcbiAgbGF5ZXJzOiAtPlxuICAgIHJlZ2lzdHJ5XG5cbiAgIyBJdGVyYXRlIG92ZXIgYWxsIG9mIHRoZSBMYXllcnMgcmVnaXN0ZXJlZCB3aXRoIEZsdWlkRnJhbWVyIGFuZCByZWZyZXNoIHRoZSBzaXplIGFuZCBwb3NpdGlvbiBvZiBlYWNoLiBcbiAgX3Jlc3BvbmQ6IC0+XG4gICAgc2VsZiA9IEBcbiAgICBfLmVhY2ggcmVnaXN0cnksIChvYmosIGluZGV4KSAtPlxuICAgICAgc2VsZi5fcmVmcmVzaExheWVyIG9ialxuXG4gICMgVGhpcyBpcyB3aGVyZSBpdCBhbGwgZ29lcyBkb3duLiBUaGlzIHdpbGwgZ2V0IGNhbGxlZCBmb3IgZWFjaCBmbHVpZCBMYXllciB3aGVuZXZlciB0aGUgYnJvd3NlciB3aW5kb3cgaXMgcmVzaXplZCwgYXMgd2VsbCBhcyB3aGVuIHRoZSBMYXllciBpcyBpbml0aWFsbHkgYWRkZWQuXG4gIF9yZWZyZXNoTGF5ZXI6IChvYmopIC0+ICBcbiAgICBsYXllciA9IG9iai50YXJnZXRMYXllclxuICAgICMgQWRqdXN0IHdpZHRoIGFuZCBoZWlnaHQsIGJhc2VkIG9uIHRoZSBhdXRvV2lkdGggYW5kIGF1dG9IZWlnaHQgb3B0aW9ucy5cbiAgICBpZiBvYmouYXV0b1dpZHRoP1xuICAgICAgbmV3V2lkdGggPSBpZiBvYmoud2lkdGhPZmZzZXQ/IHRoZW4gQF9wYXJlbnRXaWR0aChsYXllcikgKyBcbiAgICAgICAgb2JqLndpZHRoT2Zmc2V0IGVsc2UgQF9wYXJlbnRXaWR0aChsYXllcilcbiAgICAgIGxheWVyLndpZHRoID0gbmV3V2lkdGhcbiAgICAgIGxheWVyLnN0eWxlID0gXG4gICAgICAgIGJhY2tncm91bmRQb3NpdGlvbjogJ2NlbnRlcidcbiAgICBpZiBvYmouYXV0b0hlaWdodD9cbiAgICAgIG5ld0hlaWdodCA9IGlmIG9iai5oZWlnaHRPZmZzZXQ/IHRoZW4gQF9wYXJlbnRIZWlnaHQobGF5ZXIpICsgXG4gICAgICAgIG9iai5oZWlnaHRPZmZzZXQgZWxzZSBAX3BhcmVudEhlaWdodChsYXllcilcbiAgICAgIGxheWVyLmhlaWdodCA9IG5ld0hlaWdodFxuICAgICAgbGF5ZXIuc3R5bGUgPSBcbiAgICAgICAgYmFja2dyb3VuZFBvc2l0aW9uOiAnY2VudGVyJ1xuICAgICMgU2V0IGFsaWdubWVudCB3aXRoaW4gcGFyZW50LCBiYXNlZCBvbiB0aGUgeEFsaWduIGFuZCB5QWxpZ24gb3B0aW9ucy5cbiAgICBzd2l0Y2ggb2JqLnhBbGlnbiBcbiAgICAgIHdoZW4gJ2xlZnQnXG4gICAgICAgIGxheWVyLnggPSBAX3hXaXRoT2Zmc2V0IG9iaiwgMFxuICAgICAgd2hlbiAncmlnaHQnXG4gICAgICAgIG5ld1ggPSBAX3BhcmVudFdpZHRoKGxheWVyKSAtIGxheWVyLndpZHRoXG4gICAgICAgIGxheWVyLnggPSBAX3hXaXRoT2Zmc2V0IG9iaiwgbmV3WCBcbiAgICAgIHdoZW4gJ2NlbnRlcicgXG4gICAgICAgIGxheWVyLmNlbnRlclgoKVxuICAgICAgICBsYXllci54ID0gQF94V2l0aE9mZnNldCBvYmosIGxheWVyLnhcbiAgICAgIHdoZW4gJ21pZGRsZSdcbiAgICAgICAgbGF5ZXIuY2VudGVyWCgpXG4gICAgICAgIGxheWVyLnggPSBAX3hXaXRoT2Zmc2V0IG9iaiwgbGF5ZXIueFxuICAgIHN3aXRjaCBvYmoueUFsaWduXG4gICAgICB3aGVuICdib3R0b20nXG4gICAgICAgIG5ld1kgPSBAX3BhcmVudEhlaWdodChsYXllcikgLSBsYXllci5oZWlnaHRcbiAgICAgICAgbGF5ZXIueSA9IEBfeVdpdGhPZmZzZXQgb2JqLCBuZXdZXG4gICAgICB3aGVuICd0b3AnXG4gICAgICAgIGxheWVyLnkgPSBAX3lXaXRoT2Zmc2V0IG9iaiwgMFxuICAgICAgd2hlbiAnbWlkZGxlJ1xuICAgICAgICBsYXllci5jZW50ZXJZKClcbiAgICAgICAgbGF5ZXIueSA9IEBfeVdpdGhPZmZzZXQgb2JqLCBsYXllci55XG4gICAgICB3aGVuICdjZW50ZXInXG4gICAgICAgIGxheWVyLmNlbnRlclkoKVxuICAgICAgICBsYXllci55ID0gQF95V2l0aE9mZnNldCBvYmosIGxheWVyLnlcblxuICAjIElmIHRoZXJlIGFyZSB4T2Zmc2V0IG9yIHlPZmZzZXQgdmFsdWVzLCBhcHBseSB0aGVtIGJlZm9yZSByZXBvc2l0aW9uaW5nIHRoZSBMYXllci5cbiAgX3hXaXRoT2Zmc2V0OiAob2JqLCB4KSAtPlxuICAgIHggPSBpZiBvYmoueE9mZnNldD8gdGhlbiB4ICsgb2JqLnhPZmZzZXQgZWxzZSB4XG4gIF95V2l0aE9mZnNldDogKG9iaiwgeSkgLT5cbiAgICB5ID0gaWYgb2JqLnlPZmZzZXQ/IHRoZW4geSArIG9iai55T2Zmc2V0IGVsc2UgeVxuXG4gICMgQ2hlY2sgdG8gc2VlIHdoZXRoZXIgdGhlIExheWVyIGhhcyBhIHN1cGVyTGF5ZXIgb3IgaXMgb24gdGhlIHRvcCBsZXZlbCwgYW5kIHJldHVybiBlaXRoZXIgdGhlIHN1cGVyTGF5ZXIncyBkaW1lbnNpb25zIG9yIHRoZSBicm93c2VyIHdpbmRvdydzLlxuICBfcGFyZW50V2lkdGg6IChsYXllciktPlxuICAgIGlmIGxheWVyLnN1cGVyTGF5ZXI/IHRoZW4gbGF5ZXIuc3VwZXJMYXllci53aWR0aCBlbHNlIHdpbmRvdy5pbm5lcldpZHRoXG4gIF9wYXJlbnRIZWlnaHQ6IChsYXllciktPlxuICAgIGlmIGxheWVyLnN1cGVyTGF5ZXI/IHRoZW4gbGF5ZXIuc3VwZXJMYXllci5oZWlnaHQgZWxzZSB3aW5kb3cuaW5uZXJIZWlnaHRcblxuICAjIEFkZCBhIGxheWVyIHRvIHRoZSByZWdpc3RyeS5cbiAgX2FkZExheWVyOiAobGF5ZXIsIG9wdGlvbnMgPSB7fSkgLT5cbiAgICBvYmogPSBfLmV4dGVuZCBvcHRpb25zLCB0YXJnZXRMYXllcjogbGF5ZXJcbiAgICByZWdpc3RyeS5wdXNoIG9ialxuICAgICMgTGF5ZXJzIGNhbiBiZSBhZGRlZCBiZWZvcmUgdGhlIERPTSBpcyBmdWxseSBsb2FkZWQuIFNvLCB1c2UgRnJhbWVyJ3MgYGRvbUNvbXBsZXRlYCBtZXRob2QgdG8gd2FpdCB1bnRpbCB3ZSdyZSBnb29kIHRvIGdvIGJlZm9yZSBwb3NpdGlvbmluZyB0aGUgbGF5ZXIuXG4gICAgc2VsZiA9IEBcbiAgICBVdGlscy5kb21Db21wbGV0ZSAoKS0+XG4gICAgICBzZWxmLl9yZWZyZXNoTGF5ZXIgb2JqLCBzZWxmXG4gICAgbGF5ZXJcblxuICAjIFJlbW92ZSBhIGxheWVyIGZyb20gdGhlIHJlZ2lzdHJ5LlxuICBfcmVtb3ZlTGF5ZXI6IChsYXllcikgLT5cbiAgICB0YXJnZXQgPSBfLmZpbmRXaGVyZSByZWdpc3RyeSwge3RhcmdldExheWVyOiBsYXllcn1cbiAgICAjIEJ1dCBkaXRjaCBpZiB0aGUgdGFyZ2V0IGxheWVyIGlzbid0IHJlZ2lzdGVyZWQuXG4gICAgcmV0dXJuIGxheWVyIGlmICF0YXJnZXQ/XG4gICAgaWYgdGFyZ2V0LmF1dG9XaWR0aD8gb3IgdGFyZ2V0LmF1dG9IZWlnaHQ/XG4gICAgICB0YXJnZXQuc3R5bGUgPVxuICAgICAgICBiYWNrZ3JvdW5kUG9zaXRpb246ICdpbml0aWFsJ1xuICAgIHJlZ2lzdHJ5ID0gXy53aXRob3V0IHJlZ2lzdHJ5LCB0YXJnZXRcbiAgICB0YXJnZXRcblxuIyBJbml0aWFsaXplIHRoZSBGbHVpZEZyYW1lciBvYmplY3QgYXMgYEZyYW1lci5GbHVpZGAuXG5GcmFtZXIuRmx1aWQgPSBuZXcgRmx1aWRGcmFtZXIiXX0=
