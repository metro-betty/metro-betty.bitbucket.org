{TextLayer} = require 'TextLayer'

navigationButtons = [
	"Kundenansicht",
	"Bestelleingabe",
	"Aufträge",
	"Kommissionieren",
	"Rechnung",
	"Transport",
	"Transportmittel",
	"Administration",
	"Retouren",
	"Abmelden"
]

## VARIABLES

sidebarWidthFactor = 0.25; #0.25
maxFilterHeight = 200;
collapsedMaxScroll = 0;
expandedMaxScroll = 0;
useNumbersInClearAll = false

initialFilterData = [1,3,5,6,7,8,10,11,14,20]
# initialFilterData = []

filters = [
		{ title:'Einschränken auf Artikel in Order', type:'divider'},
	{ title:'Suche: Lorem ipsum', active:true , type:'search'},
		{ title:'Einschränken auf Zeitraum', active:false, type:'divider'},
	{ title:'Lieferung', date1:'10.07.2016', combinator:' – ', date2:'16.07.2016', active:true , type:'range'},
		{ title:'Probleme', type:'group', groupName:'problems', active:true},
	{ title:'Erfordert Kreditprüfung', active:true, type:'checkbox', group:'problems'},
	{ title:'Offene Preisänderungen', active:true , type:'checkbox', group:'problems'},
	{ title:'Offener Kundenkommentar', active:true , type:'checkbox', group:'problems'},
	{ title:'Offene Konsolidierungsbestätigung', active:true , type:'checkbox', group:'problems'},
	{ title:'Fehlende Transporttour', active:false , type:'checkbox', group:'problems'},
	{ title:'Hat Ersetzungen', active:true , type:'checkbox', group:'problems'},
	{ title:'Erfordert Eingreifen', active:true , type:'checkbox', group:'problems'},
	{ title:'Erfordert kein Eingreifen', active:false , type:'checkbox', group:'problems'},
		{ title:'Status', type:'group', groupName:'status', active:true},
	{ title:'In Vorbereitung', active:true , type:'checkbox', group:'status'},
	{ title:'Bestätigt', active:false , type:'checkbox', group:'status'},
	{ title:'In Kommissionierung', active:false , type:'checkbox', group:'status'},
	{ title:'Kommissioniert', active:false , type:'checkbox', group:'status'},
	{ title:'In Lieferung', active:false , type:'checkbox', group:'status'},
	{ title:'Storniert', active:false , type:'checkbox', group:'status'},
	{ title:'Geliefert', active:true , type:'checkbox', group:'status'},
	{ title:'Rechnung noch nicht gesendet', active:false , type:'checkbox', group:'status'},
	{ title:'Rechnung gesendet', active:false , type:'checkbox', group:'status'},
		{ title:'BKZ Nummern', active:false, type:'group', groupName:'bkz', active:false},
	{ title:'BKZ: 00010', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00011', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00012', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00013', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00014', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00015', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00016', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00017', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00018', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00019', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00020', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00021', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00022', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00023', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00024', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00025', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00026', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00029', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00031', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00032', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00035', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00037', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00039', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00046', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00049', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00054', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00057', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00062', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00066', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00072', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00077', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00080', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00081', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00105', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00108', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00109', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00401', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00402', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00500', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00501', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00512', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00521', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00529', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00546', active:false , type:'checkbox', group:'bkz'},
	{ title:'BKZ: 00638', active:false , type:'checkbox', group:'bkz'}
]

## FUNCTIONS


setInitialFilterData = () ->
	clearAllFilters()
	for index in  [0...initialFilterData.length]
		filters[initialFilterData[index]].active = true;
	updateFilterList(true)
	updateFilterAmount()
	sidebar.updateContent()
	updateFiltersDropdown()
	updateCheckboxAmounts()

calculateActiveFilterAmount = () ->
	amount = 0
	for index in [0...filters.length]
		if filters[index].active		
			amount += 1
			
	return amount

	
	
showAllFilters = () ->
# 	print 'showAllFilters'

	collapsedMaxScroll = filterList.scrollFrame.height
# 	print collapsedMaxScroll
	
	filterListHeight = filterListHolder.contentFrame().height + 48
	
	filterListShowMore.active = true
	filterListShowMore.rotation = 180
	
	filterList.animate
		properties:
			height: filterListHeight
		time:0.25
	
	filterListHolder.animate
		properties:
			height: filterListHeight
		time:0.25
		
	filterListGradient.animate
		properties:
			y:filterListHeight-filterListGradient.height
			opacity:0
		time:0.25
		
	filterListShowMore.animate
		properties:
			y: filterListHeight-20
		time:0.25
		
	sortResultsDropdown.animate
		properties: 
			y:filterList.y + filterListHeight + 48
		time:0.25
		
	resultInfo.animate
		properties:
			y:filterList.y + filterListHeight + 112 + 16
		time:0.25
		
	resultsList.animate
		properties: 
			y:filterList.y + filterListHeight + 144 + 16
		time:0.25
	
		
hideAllFilters = () ->
# 	print 'hideAllFilters'

	#TODO: calculate Total height after collapse and scroll to best Y
	expandedMaxScroll = filterList.scrollFrame.height
# 	print expandedMaxScroll

	filterListShowMore.active = false
	filterListShowMore.rotation = 0
	
	filterList.animate
		properties:
			height: maxFilterHeight
		time:0.25
	
	filterListHolder.animate
		properties:
			height: maxFilterHeight
		time:0.25
		
	filterListGradient.animate
		properties:
			y:maxFilterHeight-filterListGradient.height
			opacity:1
		time:0.25
		
	filterListShowMore.animate
		properties:
			y: maxFilterHeight-20
		time:0.25
		
	sortResultsDropdown.animate
		properties: 
			y:filterList.y + maxFilterHeight + 32
		time:0.25
		
	resultInfo.animate
		properties:
			y:filterList.y + maxFilterHeight + 112 + 16
		time:0.25
		
	resultsList.animate
		properties: 
			y:filterList.y + maxFilterHeight + 144 + 16
		time:0.25


clearFilter = (item) ->
# 	print 'clearFilter', item.filterID
	item.parent.animate
		properties:
			opacity:0
		time:0.5
	item.parent.on Events.AnimationEnd, ->
		filters[item.filterID].active = false
		if filterListShowMore.active
			updateFilterList(false)
		else
			updateFilterList(true)
		updateFilterAmount()
		sidebar.updateContent()
		updateFiltersDropdown()
		updateCheckboxAmounts()
		
clearAllFilters = () ->
	for index in [0...filters.length]
		filters[index].active = false
	for index in [0...filterList.items.length]
		filterList.items[index].destroy()
	updateFilterList(true)
	updateFilterAmount()
	sidebar.updateContent()
	updateFiltersDropdown()
	updateCheckboxAmounts()


updateFilterAmount = () ->
	filterAmount = calculateActiveFilterAmount()
	filterDropdownAmount.html = filterAmount
	if filterAmount <= 0
		filterDropdownAmount.visible = false
	else 
		filterDropdownAmount.visible = true
	
	
updateFilterList = (initial) ->

	for index in [0...filterList.items.length]
		filterList.items[index].destroy()
	
	filterList.items = []
	
	for index in [0...filters.length]
		if filters[index].active		
			filterItem = new Layer
				parent:filterListHolder
				width:sidebar.width - 32
				height:36
				y: filterList.items.length*40
				borderWidth: 1
				backgroundColor: 'transparent'
				borderColor: '#ffffff'
				html:filters[index].title
				style:
					margin:'16px'
					fontFamily: 'Roboto-Regular'
					fontSize:'14px'
					color:'#ffffff'
					paddingTop:'2px'
					paddingLeft:'12px'
			filterItemClear = new Layer
				parent:filterItem
				width:24
				height:28
				backgroundColor: 'transparent'
				html:"j"
				style:
					fontSize: '15px'
					color: '#ffffff'
					paddingTop:'0px'
					textAlign:'center'
					fontFamily: 'untitled-font-14'
			filterItemClear.filterID = index;	
			filterItemClear.fluid
				xAlign:'right'
				xOffset:-6
				yAlign:'center'
				yOffset:0
			filterItemClear.on Events.Click, ->
	# 			print 'clearFilter ' + this.filterID
				clearFilter( this )
			filterList.items.push( filterItem );
			
			if(filters[index].type == 'range')
				filterItem.html = filters[index].title + ': ' + filters[index].date1 + filters[index].combinator + filters[index].date2
	

	if filterListHolder.contentFrame().height > maxFilterHeight
		
		if initial
			filterList.height = maxFilterHeight
			filterListHolder.height = maxFilterHeight
			filterListGradient.visible = true
			filterListGradient.opacity = 1
			filterListGradient.y = filterList.height-filterListGradient.height + 16
			filterListShowMore.active = false
			filterListShowMore.visible = true
			filterListShowMore.y = filterList.height-20
			filterListShowMore.rotation = 0
			sortResultsDropdown.y = filterList.y + filterList.height + 48
			resultInfo.y = filterList.y + filterList.height + 112 + 16
			resultsList.y = filterList.y + filterList.height + 144 + 16
		else
			filterListHeight = filterListHolder.contentFrame().height + 48
			filterList.height = filterListHeight + 16 
			filterListHolder.height = filterListHeight + 16
			filterListGradient.y = filterListHeight-filterListGradient.height
			filterListShowMore.y = filterListHeight-20
			sortResultsDropdown.y = filterList.y + filterList.height + 48
			resultInfo.y = filterList.y + filterList.height + 112 + 16
			resultsList.y = filterList.y + filterList.height + 144 + 16
	else 
		if filterListHolder.children.length > 0
			filterList.height = filterListHolder.contentFrame().height + 16 
			filterListHolder.height = filterListHolder.contentFrame().height + 16
			sortResultsDropdown.y = filterList.y + filterList.height + 48
			resultInfo.y = filterList.y + filterList.height + 112 + 16
			resultsList.y = filterList.y + filterList.height + 144 + 16
		else
			filterList.height = 0
			filterListHolder.height = 0
			sortResultsDropdown.y = filterList.y + filterList.height
			resultInfo.y = filterList.y + filterList.height + 80
			resultsList.y = filterList.y + filterList.height + 112


updateFiltersDropdown = () ->
# 	print 'update filters dropdown'
	for groupIndex in [0...filterDropdownContent.children.length]
		form = filterDropdownContent.children[ groupIndex ]
		if form.type == 'group'
# 			print 'group', form
			
			if form.active
# 				print 'group is active', form.contentFrame().height
				form.height = form.contentFrame().height
				form.children[0].scaleY=-1
			else
				form.height = 48;
			
			for index in [0...form.children.length]
				childForm = form.children[index]
				if childForm.type == 'checkbox'
					if filters[childForm.formID].active == true
						childForm.html = '<input style="position:relative; top:0px" type="checkbox" value="value" checked>'
					else
						childForm.html = '<input style="position:relative; top:0px" type="checkbox" value="value">'
		if form.type=='range'
# 			print 'is range'
			if filters[form.formID].active == true
# 				print 'range is active'
				form.html = '<input style="position:relative; top:12px" type="checkbox" value="value" checked>'
			else
# 				print 'range is inactive' 
				form.html = '<input style="position:relative; top:12px" type="checkbox" value="value">'


updateCheckboxAmounts = () ->
	
	for groupIndex in [0...filterDropdownContent.children.length]
	
		numChildren = 0;
		activeChildren = 0;
	
		group = filterDropdownContent.children[ groupIndex ]
		if group.type == 'group'
		
			for childIndex in [0...filters.length]
				if filters[childIndex].group == filters[ group.formID ].groupName
					numChildren += 1
					if filters[childIndex].active 
						activeChildren += 1
			
			group.html = filters[ group.formID ].title + ' (' + activeChildren + '/' + numChildren + ')'
			

## HEADER

header = new Layer
	backgroundColor: "#476297"
	width: Screen.width
	height: 72
	
logo = new Layer
	parent: header
	height:72
	width:Screen.width*sidebarWidthFactor
	backgroundColor: '#1a3b7a'
	html: "r"
	style:
		fontSize: '100px'
		color: '#fbe400'
		paddingTop:'20px'
		textAlign:'center'
		fontFamily: 'untitled-font-14'
	
navigation = new Layer
	parent: header
	backgroundColor: 'transparent'
	clip:false


home = new TextLayer
	parent:navigation
	autoSize:true
	fontFamily:"untitled-font-14"
	text:"G"
	y:22
	paddingRight:12
	color: '#becede'

for index in [navigationButtons.length-1 ... 0]
	button = new TextLayer
		parent: navigation
		autoSize: true
		backgroundColor: 'transparent'
		height:72
		fontFamily: 'Roboto-Regular'
		text: navigationButtons[index]
		color: '#becede'
		x: navigation.contentFrame().width
		fontSize:14
		paddingTop: 26
		paddingRight:24

navigation.width = navigation.contentFrame().width;
navigation.fluid
	autoHeight:true
	xAlign: 'right'
	
## SIDEBAR
	
sidebar = new ScrollComponent
	y:72
	width:Screen.width*sidebarWidthFactor
	height:Screen.height-header.height
	backgroundColor: '#233452'
	scrollHorizontal: false

## ATTENTION BAR

attention = new Layer
	parent: sidebar.content
	backgroundColor: '#ff5916'
	width:sidebar.width
	height:56
	html:'6378 Aufträge benötigen<br>sofortige Aufmerksamkeit'
	style:
		fontFamily: 'Roboto-Regular'
		fontSize:'14px'
		lineHeight:'18px'
		paddingTop:'9px'
		paddingLeft:'50px'

attentionIcon = new Layer
	parent:attention
	backgroundColor:'transparent'
	width:52
	height:56
	html:"J"
	style:
		fontSize: '21px'
		color: '#ffffff'
		paddingTop:'18px'
		textAlign:'center'
		fontFamily: 'untitled-font-14'
		
attentionButton = new Layer
	parent:attention
	height:32
	width:80	
	borderRadius: 3
	borderColor: '#ffffff'
	borderWidth: 1
	backgroundColor: 'transparent'
	html:'anzeigen'
	style:
		fontFamily: 'Roboto-Regular'
		fontSize:'14px'
		lineHeight:'18px'
		paddingTop:'5px'
		textAlign:'center'

attentionButton.on Events.Click, ->
	setInitialFilterData()
	
attentionButton.fluid
	xAlign:'right'
	xOffset:-10
	yAlign:'center'
	yOffset:-1

search = new Layer
	parent: sidebar.content
	y:sidebar.content.contentFrame().height
	width:sidebar.width - 32
	height:32
	backgroundColor: '#1b2940'
	borderRadius: 3
	html:'Suchen'
	style:
		fontFamily: 'Roboto-Regular'
		fontSize:'14px'
		color:'#7d9ebf'
		paddingLeft:'8px'
		paddingTop:'2px'
		margin:'16px'
		
searchIcon = new Layer
	parent:search
	backgroundColor:'transparent'
	width:52
	height:56
	html:"z"
	style:
		fontSize: '15px'
		color: '#ffffff'
		paddingTop:'3px'
		textAlign:'center'
		fontFamily: 'untitled-font-14'
searchIcon.fluid
	xAlign:'right'	
	xOffset:10
	
## FILTER DROPDOWN

filterDropdown = new Layer
	parent: sidebar.content
	y:sidebar.content.contentFrame().height + 32
	backgroundColor: 'transparent'
	height:64
	width: sidebar.width
	html:'Filter'
	active:false
	style:
		fontFamily: 'Roboto-Regular'
		fontSize:'14px'
		color:'#ffffff'
		paddingLeft:'44px'
		paddingTop:'17px'

filterDropdownHit = new Layer
	parent:filterDropdown
	width:sidebar.width-100
	height:64
	backgroundColor: 'transparent'

filterDropdownHit.on Events.Click, ->
	if( filterDropdown.active == true )
# 		print 'close'
		filterDropdown.active = false
		
		filterDropdownIcon.rotation = 0;
		filterDropdownIcon.style.paddingLeft = '8px'
		filterDropdownIcon.x = 0
		
		filterDropdownContent.animate
			properties:
				height:0
			time:0.25
		filterDropdownContent.on Events.AnimationEnd, ->
			filterDropdownContent.off Events.AnimationEnd
			sidebar.updateContent()
	else
# 		print 'open'
		filterDropdown.active = true
		filterDropdownIcon.rotation = 90;
		filterDropdownIcon.style.paddingLeft = '0px'
		filterDropdownIcon.x = 4
		
		filterDropdownContent.animate
			properties:
				height:filterDropdownContent.contentFrame().height
			time:0.25
		filterDropdownContent.on Events.AnimationEnd, ->
			filterDropdownContent.off Events.AnimationEnd
			sidebar.updateContent()
	
filterDropdownIcon = new Layer
	parent:filterDropdown
	backgroundColor:'transparent'
	width:40
	height:40
	html:"d"
	style:
		fontSize: '21px'
		color: '#ffffff'
		paddingTop:'5px'
		paddingLeft:'8px'
		textAlign:'center'
		fontFamily: 'untitled-font-14'
filterDropdownIcon.fluid
	yAlign:'center'

filterDropdownAmount = new Layer
	parent:filterDropdown
	borderRadius: 16
	height: 28
	width:58
	backgroundColor:'#1b2940'
	html:calculateActiveFilterAmount()
	style:
		fontFamily: 'Roboto-Regular'
		fontSize:'14px'
		color:'#ffffff'
		paddingTop:'0px'
		paddingLeft:'12px'
		paddingRight:'28px'
		textAlign:'center'
filterDropdownAmount.fluid
	xAlign:'right'
	xOffset:-16
	yAlign:'center'
	
if useNumbersInClearAll == false
	filterDropdownAmount.backgroundColor = 'transparent'
	filterDropdownAmount.style.color='transparent'
	filterDropdownAmount.fluid
		xAlign:'left'
		xOffset:52
	
filterDropdownAmountClear = new Layer
	parent:filterDropdownAmount
	backgroundColor:'transparent'
	width:28
	height:28
	html:"j"
	style:
		fontSize: '10px'
		color: '#ffffff'
		paddingTop:'0px'
		paddingLeft:'36px'
		textAlign:'center'
		fontFamily: 'untitled-font-14'

filterDropdownAmountClear.on Events.Click, ->
	clearAllFilters()


filterList = new Layer
	parent: sidebar.content
	y:sidebar.content.contentFrame().height
	width:sidebar.width
	height:36
	backgroundColor:'transparent'
	clip:true
filterList.items = []

filterListHolder = new Layer
	parent:filterList
	width:sidebar.width
	backgroundColor:'transparent'

filterListGradient = new Layer
	parent:filterList
	width:sidebar.width
	height:64
	visible:false
	style:
		background:"-webkit-linear-gradient(top, rgba(35,52,82,0) 0%, rgba(35,52,82,255) 100%)"
	
filterListShowMore = new Layer
	parent:filterList
	width:24
	height:28
	visible:false
	backgroundColor: 'transparent'
	html:"b"
	style:
		fontSize: '21px'
		color: '#ffffff'
		paddingTop:'0px'
		textAlign:'center'
		fontFamily: 'untitled-font-14'
filterListShowMore.active = false
filterListShowMore.fluid
	xAlign:'center'	
filterListShowMore.on Events.Click, ->
# 	print 'show/hide all filters ', filterListShowMore.active
	sidebar.scrollToPoint(
		x:0, y:0
		true
	)
	if filterListShowMore.active == false
		showAllFilters()
	else
		hideAllFilters()

	
## SORT RESULTS DROPDOWN
	
sortResultsDropdown = new Layer
	parent: sidebar.content
	y:sidebar.content.contentFrame().height + 32
	width:sidebar.width - 32
	height:32
	backgroundColor: '#1b2940'
	borderRadius: 3
	html:'Sortieren nach Lieferdatum & Kundenname'
	style:
		fontFamily: 'Roboto-Regular'
		fontSize:'14px'
		color:'#7d9ebf'
		paddingLeft:'8px'
		paddingTop:'2px'
		margin:'16px'
		

sortResultsDropdownIcon = new Layer
	parent:sortResultsDropdown
	width:24
	height:28
	backgroundColor: 'transparent'
	html:"b"
	style:
		fontSize: '15px'
		color: '#ffffff'
		paddingTop:'0px'
		textAlign:'center'
		fontFamily: 'untitled-font-14'

sortResultsDropdownIcon.fluid
	xAlign:'right'
	xOffset:-6
	yAlign:'center'
	yOffset:0

## RESULTS

resultInfo = new Layer
	parent:sidebar.content
	y:sidebar.content.contentFrame().height + 48
	width:sidebar.width
	height:32
	backgroundColor: 'transparent'
	html:'6.356 Aufträge'
	style:
		fontFamily: 'Roboto-Regular'
		fontSize:'14px'
		color:'#7d9ebf'
		paddingLeft:'16px'
		
resultsList = new Layer
	parent:sidebar.content
	y:sidebar.content.contentFrame().height
	width:sidebar.width
	backgroundColor: 'transparent'
resultsList.on Events.AnimationEnd, ->
		sidebar.updateContent()
			
resultItem = new Layer
	parent:resultsList
	height:64
	width:sidebar.width
	y:resultsList.contentFrame().height;
	backgroundColor:'#1b2940'
resultItemImage = new Layer
	parent:resultItem
	width:310
	height:64
	image:'images/resultItem_01.png'

for index in [0...15]
	resultItem2 = new Layer
		parent:resultsList
		width:sidebar.width
		height:80
		backgroundColor: '#223353'
		y:resultsList.contentFrame().height + 1;
	resultItem2Image = new Layer
		parent:resultItem2
		width:310
		height:80
		image:'images/resultItem_02.png'

resultsList.height = resultsList.contentFrame().height;
	
## FILTER DROPDOWN CONTENT

filterDropdownContent = new Layer
	parent:sidebar.content
	y:filterList.y
	width:sidebar.width
	height:0
	clip:true
	backgroundColor:'#476297'
	shadowY: 25
	shadowBlur: 25
	shadowColor: 'rgba(0,0,0,0.25)'
	
filterDropdownOffsetY = -16

isGroup = false
groupHolder = null

for index in [0...filters.length]

	if filters[index].type == "checkbox"
		
		form = ''
		if filters[index].active
			form = '<input style="position:relative; top:0px" type="checkbox" value="value" checked>'
		else
			form = '<input style="position:relative; top:0px" type="checkbox" value="value">'
			
		checkbox = new Layer
			parent:filterDropdownContent
			width:sidebar.width
			y:filterDropdownContent.contentFrame().height + filterDropdownOffsetY
			height:26
			backgroundColor:'transparent'
			html: form
			style:
				paddingLeft:'16px'
				paddingTop:'0px'
		checkbox.formID = index
		checkbox.type = "checkbox"	
		checkbox.active = filters[index].active
		checkbox.on Events.Click, ->
			if filters[ this.formID ].active == true
# 				print 'set checkbox inactive', this.formID
				filters[ this.formID ].active = false
				checkbox.active = false
				this.html = '<input style="position:relative; top:0px" type="checkbox" value="value">'
			else
# 				print 'set checkbox active', this.formID
				filters[ this.formID ].active = true
				checkbox.active = true
				this.html = '<input style="position:relative; top:0px" type="checkbox" value="value" checked>'
			updateFilterList(true)
			updateFilterAmount()
			updateCheckboxAmounts()
			sidebar.updateContent()
			
		checkboxLabel = new Layer
			parent: checkbox
			html:filters[index].title
			width:sidebar.width - 36
			height:24
			backgroundColor:'transparent'
			style:
				fontFamily: 'Roboto-Regular'
				fontSize:'14px'
				color:'#ffffff'
				paddingLeft:"36px"
				paddingTop:'2px'
		
		if filters[index].group != undefined
# 			print 'checkbox in group'
			group = filterDropdownContent.childrenWithName( 'group_' + filters[index].group )[0];
			checkbox.parent = group
			checkbox.y = 24 * ( group.children.length ) - 8
			if group.active
				group.height = group.contentFrame().height + 16
			else group.height = 48
	
	if filters[index].type == "search"
# 		print 'adding a search'
		searchWrapper = new Layer
			parent:filterDropdownContent
			width:sidebar.width
			height:48
			backgroundColor:'transparent'
			y:filterDropdownContent.contentFrame().height + filterDropdownOffsetY
		searchWrapper.formID = index
		searchWrapper.type = "search"
		
		search = new Layer
			parent:searchWrapper
			width:sidebar.width - 32
			height:32
			backgroundColor:'1c293f'
			borderRadius: 3
# 			html:'Nach Artikeln suchen'
			html:'Lorem ipsum'
			style:
				fontFamily: 'Roboto-Regular'
				fontSize:'14px'
# 				color:'#7d9ebf'
				color:'#ffffff'
				paddingLeft:'8px'
				paddingTop:'2px'
				margin:'16px'
				
		searchIcon = new Layer
			parent:search
			backgroundColor:'transparent'
			width:52
			height:56
			html:"z"
			style:
				fontSize: '15px'
				color: '#ffffff'
				paddingTop:'3px'
				textAlign:'center'
				fontFamily: 'untitled-font-14'
		searchIcon.fluid
			xAlign:'right'	
			xOffset:10
			
	if filters[index].type == "range"
# 		print 'adding a range'
		form = ''
		if filters[index].active
			form = '<input style="position:relative; top:12px" type="checkbox" value="value" checked>'
		else
			form = '<input style="position:relative; top:12px" type="checkbox" value="value">'
			
		rangeWrapper = new Layer
			parent:filterDropdownContent
			width:sidebar.width
			y:filterDropdownContent.contentFrame().height + filterDropdownOffsetY
			height:48
			backgroundColor:'transparent'
			html: form
			style:
				paddingLeft:'16px'
				paddingTop:'0px'
		rangeWrapper.formID = index
		rangeWrapper.type = "range"
		rangeWrapper.active = filters[index].active
		
		rangeWrapper.on Events.Click, ->
			if filters[ this.formID ].active == true
# 				print 'set checkbox inactive', this.formID
				filters[ this.formID ].active = false
				this.active = false
				this.html = '<input style="position:relative; top:12px" type="checkbox" value="value">'
			else
# 				print 'set checkbox active', this.formID
				filters[ this.formID ].active = true
				this.active = true
				this.html = '<input style="position:relative; top:12px" type="checkbox" value="value" checked>'
			updateFilterList(true)
			updateFilterAmount()
			sidebar.updateContent()
			
		rangeWrapperLabel = new Layer
			parent: rangeWrapper
			html:filters[index].title
			width:sidebar.width - 36
			height:24
			backgroundColor:'transparent'
			style:
				fontFamily: 'Roboto-Regular'
				fontSize:'14px'
				color:'#ffffff'
				paddingLeft:"36px"
				paddingTop:'18px'

		rangeSearch1 = new Layer
			parent:rangeWrapper
			x:92
			width:92
			height:32
			backgroundColor:'1c293f'
			borderRadius: 3
			html:filters[index].date1
			style:
				fontFamily: 'Roboto-Regular'
				fontSize:'14px'
# 				color:'#7d9ebf'
				color:'#ffffff'
				paddingLeft:'8px'
				paddingTop:'2px'
				margin:'16px'
		
		rangeCombinator = new Layer
			parent: rangeWrapper
			backgroundColor:'transparent'
			width:32
			height:32
			x:190
			html:filters[index].combinator
			style:
				fontFamily: 'Roboto-Regular'
				fontSize:'14px'
				color:'##ffffff'
				paddingLeft:'8px'
				paddingTop:'2px'
				margin:'16px'
		
		rangeSearch2 = new Layer
			parent:rangeWrapper
			x:220
			width:92
			height:32
			backgroundColor:'1c293f'
			borderRadius: 3
			html:filters[index].date2
			style:
				fontFamily: 'Roboto-Regular'
				fontSize:'14px'
# 				color:'#7d9ebf'
				color:'#ffffff'
				paddingLeft:'8px'
				paddingTop:'2px'
				margin:'16px'
				
		
		
	if filters[index].type == "divider"
# 		print 'adding a divider'
		divider = new Layer
			parent:filterDropdownContent
			width:sidebar.width
			height:48
			backgroundColor:'transparent'
			y:filterDropdownContent.contentFrame().height + filterDropdownOffsetY
			html: filters[ index ].title
			name:'divider' + index
			style:
				fontFamily: 'Roboto-Bold'
				fontSize:'12px'
				color:'#7d9ebf'
				paddingLeft:'16px'
				paddingTop:'8px'
				paddingBottom:'20px'
				marginTop:'16px'
				borderTopWidth:'1px'
				borderColor:'#243551'
		divider.formID = index
		divider.type = 'divider'
	
	if filters[index].type == "group"
# 		print 'adding a group'
		groupDropdown = new Layer
			parent:filterDropdownContent
			width:sidebar.width
			height:48
			backgroundColor:'transparent'
			y:filterDropdownContent.contentFrame().height + filterDropdownOffsetY
			name:'group_' + filters[index].groupName
			clip: true
			style:
				fontFamily: 'Roboto-Bold'
				fontSize:'12px'
				color:'#7d9ebf'
				paddingLeft:'16px'
				paddingTop:'10px'
				paddingBottom:'0px'
				marginTop:'16px'
				borderTopWidth:'1px'
				borderColor:'#243551'
		groupDropdown.active = filters[index].active
		groupDropdown.formID = index
		groupDropdown.html = filters[ index ].title
		
		groupDropdownArrow = new Layer 
			parent:groupDropdown
			backgroundColor: 'transparent'
			width:sidebar.width
			height:48
			html:"b"
			name:'arrow'
			style:
				fontSize: '15px'
				color: '#ffffff'
				paddingTop:'10px'
				textAlign:'right'
				paddingRight:'16px'
				fontFamily: 'untitled-font-14'
		groupDropdownArrow.fluid
			xAlign:'right'
			
		groupDropdownArrow.on Events.Click, ->
			
			dropdown = this.parent
			
			if dropdown.active == true
				
# 				print '##### close #####'
				
				this.scaleY = 1
				dropdown.active = false
				dropdown.animate
					properties:
						height:48
					time:0.25
				
				yCorrect = 0;
				if dropdown.index < filterDropdownContent.children.length
					yCorrect = -16
				
				filterDropdownContent.animate
					properties:
						height:filterDropdownContent.contentFrame().height - dropdown.contentFrame().height + 48 + yCorrect
					time:0.25
						
				for index in [0...dropdown.siblings.length]
					sibling =  dropdown.siblings[index]
					if sibling.y > dropdown.y
						offsetY = sibling.y - dropdown.y - dropdown.contentFrame().height + 32
						sibling.animate
							properties:
								y:dropdown.y + offsetY
							time:0.25
			else
				
# 				print '##### open #####'
				
				dropdown.active = true
				this.scaleY = -1
				dropdown.animate
					properties:
						height: dropdown.contentFrame().height
					time:0.25
				
				filterDropdownContent.animate
					properties:
						height:filterDropdownContent.contentFrame().height + dropdown.contentFrame().height - 32
					time:0.25
					
				for index in [0...dropdown.siblings.length]
					sibling =  dropdown.siblings[index]
					if sibling.y > this.parent.y
						offsetY = sibling.y - dropdown.y
						sibling.animate
							properties:
								y:dropdown.y + dropdown.contentFrame().height + offsetY - 32
							time:0.25

			
		groupDropdown.formID = index
		groupDropdown.type = 'group'
		

 
		
## STARTUP
setInitialFilterData()